\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{courier}
\usepackage{mathptmx}
\usepackage{sectsty}
\usepackage{changepage}
\usepackage{tikz}
\usepackage{color}
\usepackage{filecontents}
\usepackage[hidelinks]{hyperref}
\usepackage{csquotes}
\usepackage[backend=bibtex]{biblatex}
\usepackage{float}
\usepackage{listings}
\usepackage{tabularx}
%\usepackage{framed}
\usepackage{float}
\usepackage{subcaption}
\usepackage[czech]{babel}

\renewcommand{\figurename}{Obr.}
\renewcommand{\contentsname}{Obsah}

\graphicspath{{./images/}}

\newcommand{\todo}[1]{\textcolor{red}{\textbf{[[#1]]}}}

\DeclareCaptionType{figtext}[Text][Seznam textů]
\floatstyle{boxed} 
\restylefloat{figtext}

%\begin{filecontents}{doc.bib}
%\end{filecontents}
\addbibresource{doc.bib}

\title{Změna obrazu přiložením čočky}
\author{Tomáš Polášek (xpolas34)}
\date{\today}

\begin{document}

\begin{titlepage}
\huge
\noindent
\center{\textsc{\LARGE VYSOKÉ UČENÍ TECHNICKÉ V BRNĚ}}
\center{\textsc{\Large FAKULTA INFORMAČNÍCH TECHNOLOGIÍ}}
\newline
\begin{figure}[h]
\begin{center}
  \scalebox{0.4}
  {
    \includegraphics[scale=0.3]{FIT_logo}
  }
  \label{pic:fit_logo}
\end{center}
\end{figure}

\center{Projekt do předmětu ZPO}
\center{MIT1 2017/2018}
% Make title without page break.
{\let\newpage\relax\maketitle}
% Reset the page style
\thispagestyle{empty}

\end{titlepage}

\section{Zadání}

Cílem tohoto projektu bylo vytvořit okenní aplikace, která bude uživateli umožňovat modifikaci obrazu čočkou. Původní zadání lze vidět v rámečku \ref{Text:Task}.

\begin{figtext}[H]
    Jedná se o změnu obrazu přiložením čočky (příp. soustavou čoček) - spojky, rozptylky, možnost nastavení parametrů čočky (poloměr křivosti, index lomu).
Bude se jednat o okenní aplikaci, kde uživatel bude moci ovlivňovat nastavení modelu soustavy obrázek/čočka(y).
    \caption{Originální znění zadání projektu.}
	\label{Text:Task}
\end{figtext}

\noindent
Díky jeho volnosti bylo zadání specifikováno následujícím způsobem: 

\begin{itemize}
	\item Čočka je zobecněna na \textit{průhledný}, který tvoří pro každý paprsek maximálně 2 rozhraní\footnote{Každý paprsek, který do něj vnikne (první rozhraní) z něj opět vyjde pouze jednou (druhé rozhraní). }. Mezi jeho vlastnosti patří tvar, který je definován polygonální sítí, index lomu a případně Abbeho číslo.
    \item Tvar čočky udává její funkci -- model spojky se chová ve scéně jako spojka.
    \item Obraz, který je čočkou modifikován je umístěn na virtuálním plátně.
    \item Pozorovatel scény je kamera, která umožňuje pozorování výsledku za použitím perspektivy (simulace lidského oka).
\end{itemize}

\begin{table}[H]
  \centering
  \caption{Členové řešitelského týmu}
  \label{Tab:Team}
  \begin{tabular}{|l|l|l|}
    \hline
    \multicolumn{1}{|c|}{Jméno a příjmení} & \multicolumn{1}{c|}{Login} & \multicolumn{1}{c|}{E-mail} \\ \hline
    Tomáš Polášek                          & xpolas34                   & xpolas34@stud.fit.vutbr.cz  \\ \hline
  \end{tabular}
\end{table}

\section{Popis řešení}

Tato část textu pojednává o samotném způsobu řešení zadání, které bylo popsáno výše. První část popisuje použité technologie, následuje popis metody transformace obrazu.

\subsection{Použité technologie}

Projekt byl primárně realizován v programovacím jazyce C++. Aplikaci lze dále rozdělit do dvou částí. První z nich je samotná okenní aplikace a prvky uživatelského rozhraní. Implementace těchto částí byla provedena pomocí knihovny \textit{Qt}\cite{Qt}. Kromě výše zmíněných funkcí umožňuje \textit{Qt} také multiplatformní přístup ke grafické kartě skrz knihovnu \textit{OpenGL}. 

Druhou částí aplikace je samotná transformace obrazu, k čemuž bylo použito API grafické karty zpřístupněné pomocí knihovny \textit{OpenGL}\cite{OpenGL}. Programy běžící na grafické kartě byly napsány v jazyce \textit{GLSL}\cite{GLSL}.

\subsection{Transformace obrazu}

Obraz je transformován v reálném čase a proto bylo nutné použít metodu, která je dostatečně rychlá. Zvolená metoda pracuje na principu \textit{sledování paprsku} a \textit{depth peeling}\cite{DepthPeeling}. Základní schéma metody je následující: 
\begin{enumerate}
	\item Vykreslení zadní části čočky do pomocného \textit{framebufferu}.
    \item Vykreslení plátno se zdrojovým obrazem.
    \item Vykreslení přední části čočky a provedení sledování paprsku mezi zadní a přední vrstvou.
\end{enumerate}

Technické řešení, za pomocí grafické karty, ale není úplně přímočaré. Existuje mnoho různých implementací této metody, přičemž tato práce je inspirována článkem \uv{Real-Time Refraction Through Deformable Objects}\cite{DeformableRefraction}. 

V prvním kroku algoritmu je nutné zachovat informace o vzdálenosti daného fragmentu a normále. Tohoto je docíleno použitím textury se čtyřmi složkami -- tři složky pro normálu a jedna pro vzdálenost. Obraz zadní části čočky je vykreslován pomocí stejné transformační matice, včetně perspektivy, kterou používá kamera.

Výsledkem druhého kroku je vykreslená scéna, která obsahuje pouze nemodifikované plátno se zdrojovým obrazem. Přes tento obraz je v dalším kroku vykreslena deformace. 

Vstupem posledního kroku je textura vytvořená v prvním kroku, která obsahuje vzdálenost a normálu zadní části čočky a samotný zdrojový obraz. Mezi další důležité parametry patří také indexy lomu prostředí a materiálu čočky\footnote{Případně 3 indexy lomu, pokud je povolen efekt chromatické aberace.} a inverzní matice zdrojového plátna. Program, běžící na grafické kartě, postupuje následovně: 
\begin{enumerate}
	\item Vypočítá paprsek vycházející z kamery.
	\item Vypočítá bod refrakce na přední stěně čočky.
    \item Provede refrakci na prvním rozhraní (vně $\rightarrow$ čočka). V tomto kroku se také počítá \textit{Fresnelovu} odrazivost\cite{Fresnel} materiálu pomocí \textit{Schlickovy} aproximace \cite{SchlickApprox}.
    \item Pomocí lomeného paprsku, který byl získán v minulém kroku, nalezne průnik se zadní stěnou čočky.
    \item Provede refrakci na druhém rozhraní (čočka $\rightarrow$ vně). V tomto kroku je proveden druhý výpočet odrazivosti.
    \item Pomocí inverzní matice zdrojového obrazu transformuje výsledný bod refrakce a paprsek do prostoru obrazu a vypočítá pozici ve zdrojové textuře.
\end{enumerate}

Zajímavou částí celého algoritmu je samotná refrakce. Samotný výpočet lomeného paprsku je výsledkem zabudované funkce jazyka \textit{GLSL} jménem \textbf{refract}, která používá \textit{Snellův zákon} lomu \cite{SnellsLaw}. 

Nejdůležitější částí je však výpočet průniku, který je realizován použitím binárního vyhledávání. Parametrem vyhledávání je délka lomeného paprsku, při které se nejvíce přiblíží zadní stěně čočky. Při každé iteraci je vyhodnoceno, jestli je paprsek s danou délkou blíže, nebo dále než zadní stěna čočky. Vyhledávací interval je následně zkrácen tak, aby algoritmus konvergoval k výsledku. Počet iterací byl experimentálně nastaven na hodnotu 10.

Na závěr je získaná barva ze zdrojové textury násobena inverzní hodnotou ($1 - Odrazivost$) obou získaných odrazivostí, čímž efekt čočky získá realističtější vzhled.

Kromě základního vykreslení, které je popsáno výše je také možné zapnout efekt chromatické aberace, kdy jsou jednotlivé barevné složky vzájemně posunuty. Parametr posunutí, který lze nastavit, je udáván tzv. \textit{Abbeho číslem}\cite{AbbeNumber}. Tato funkcionalita byla do aplikace přidána jako součást projektu do předmětu Fyzikální Optika (FYO), za pomocí knihy \textit{Optics} od E. Hechta \cite{OpticsHecht}. 

\section{Ovládání aplikace}

Výsledný program byl realizován jako okenní aplikace, jejíž základní rozvržení lze vidět na obr. \ref{Obr:AppWindow}. Mezi hlavní ovládací prvky patří: lišta s menu, zobrazovací plocha a záložky s nastavením

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{MainApp}
    \caption{Základní rozvržení uživatelského rozhraní.}
    \label{Obr:AppWindow}
\end{figure}

Menu lišta obsahuje zřídka používané funkce, u kterých není nutné, aby k nim měl uživatel přímý přístup. První z těchto funkcí je načtení obrázku z disku zařízení, na kterém aplikace běží. Po načtení je obrázek rovnou zobrazen na virtuálním plátně. Speciálním případem předchozí funkce je resetování obrázku do výchozího stavu. Poslední důležitou funkcí je uložení zobrazovaného výsledku na disk. 

Zobrazovací plocha je ústřední částí celé aplikace a proto je jí vyhrazena většina místa. Zobrazuje aktuální rozložení scény z pohledu kamery. Na obrázku \ref{Obr:AppWindow} lze vidět testovací scénu -- výchozí kostkovaný obrázek a čočku. Kromě zobrazování lze tuto plochu také použít k ovládání prvků scény pomocí myši. Tahem myši se zmáčknutým primárním tlačítkem je možné čočku rotovat. Kolečkem je potom možné čočku posouvat směrem ke a od kamery.

Důležitou částí jsou také konfigurační záložky, které obsahují prvky uživatelského rozhraní pomocí kterých lze měnit parametry scény. Tyto záložky obsahují: 

\begin{itemize}
	\item Konfiguraci obrazu: Velikost plátna, jeho rotaci a pozici.
    \item Konfiguraci kamery: Pozici a zorné pole.
    \item Konfiguraci čočky: Velikost čočky, její natočené a pozici. Mezi další parametry čočky patří také index lomu, přepínač chromatické aberace a případně \textit{Abbeho číslo}. Důležitou částí je také tvar čočky, přičemž je na výběr z kostky, parametrizovatelné sférické čočky a modelu definovaného souborem ve formátu \textit{PLY}.
    \item Konfigurace prostředí: Index lomu.
    \item Ovládání vstupu z web-kamery: Zapnutí/vypnutí, pořízení jednoho obrazu a vstup videa.
\end{itemize}

Součástí tvarů je také parametrizovatelná sférická čočka, u které lze zadat poloměry křivosti, poloměry ploch a její tloušťku. Další možností generátoru je také počet sektorů a kružnic, čímž lze určit členitost/hladkost výsledného objektu. Příklad vygenerované sférické čočky a chromatické aberace lze vidět na obr. \ref{AdvancedOutput}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{AdvancedOutput}
    \caption{Ukázka výstupu aplikace s modifikovanou sférickou čočkou a chromatickou aberací.}
    \label{Obr:AdvancedOutput}
\end{figure}

\section{Závěr}

Použitá metoda umožňuje realizaci efektu refrakce pro objekty definované polygonální sítí, což je vhodné např. pro počítačové hry. Metoda operuje v reálném čase na integrované grafické kartě a není tedy příliš náročná na výpočetní prostředky. Výhodou tohoto přístupu je možnost vykreslení scény bez jakýchkoliv změn a následně použít předvedenou metodu pro \uv{dokreslení} požadovaného efektu. Výsledný efekt je možné dále vylepšit výpočtem odražených paprsku a přimícháním jejich barevných složek k těm lomeným.

Výsledná aplikace umožňuje uživateli jednoduše konfigurovat scénu a aplikovat různé efekty pomocí parametrizovatelných čoček. Výhodou použití toolkitu \textit{Qt} je možnost běhu aplikace na mnoho různých platformách (testováno na Linuxu a Windows).

\pagebreak
% Rename the reference chapter to "Reference"
\renewcommand{\refname}{Reference}
\printbibliography

\end{document}
