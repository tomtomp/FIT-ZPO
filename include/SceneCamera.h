/**
 * @file SceneCamera.h
 * @author Tomas Polasek
 * @brief Scene camera class.
 */

#ifndef SCENE_CAMERA_H
#define SCENE_CAMERA_H

#include "Types.h"

#include <QVector>
#include <QMatrix4x4>
#include <QQuaternion>

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Basic Camera class, used for calculating View and Projection matrices.
    class SceneCamera
    {
    public:
        /// Initialize default values.
        SceneCamera() = default;
        ~SceneCamera() = default;

        /**
         * Set position of the camera.
         * @param pos New position of the camera.
         */
        void setPos(const QVector3D &pos);

        /**
         * Set position of the camera.
         * @param x Position on the X-axis.
         * @param y Position on the Y-axis.
         * @param z Position on the Z-axis.
         */
        void setPos(float x, float y, float z);

        /**
         * Set rotation from given quaternion.
         * @param rot New rotation of the camera.
         */
        void setRot(const QQuaternion &rot);

        /**
         * Set projection matrix to perspective projection with
         * given parameters.
         * @param fov Vertical field-of-view;
         * @param aspect Aspect ratio.
         * @param near Near clipping plane.
         * @param far Far clipping plane.
         */
        void setPerspective(float fov, float aspect, float near, float far);

        /**
         * Set projection matrix to orthographic projection with
         * given parameters.
         * @param l Left border.
         * @param r Right border.
         * @param b Bottom border.
         * @param t Top border.
         * @param near Near clipping plane.
         * @param far Far clipping plane.
         */
        void setOrtho(float l, float r, float b, float t, float near, float far);

        /**
         * Recalculate View-Projection matrix from changed matrices.
         */
        void recalculate();

        /**
         * Get the View-Projection matrix.
         * @return Returns reference to the View-Projection matrix.
         * @warning Does NOT recalculate any changes!
         */
        const QMatrix4x4 &vp() const
        { return mViewProjection; }

        /**
         * Get the View matrix.
         * @return Returns reference to the View matrix.
         * @warning Does NOT recalculate any changes!
         */
        const QMatrix4x4 &v() const
        { return mView; }

        /**
         * Get the Projection matrix.
         * @return Returns reference to the Projection matrix.
         * @warning Does NOT recalculate any changes!
         */
        const QMatrix4x4 &p() const
        { return mProjection; }
    private:
        /// Recalculate the mView matrix.
        void calculateView()
        { mView = mRotation * mTranslation; }

        /// Recalculate the mViewProjection matrix.
        void calculateViewProjection()
        { mViewProjection = mProjection * mView; }

        /// Translation matrix.
        QMatrix4x4 mTranslation;
        /// Rotation matrix.
        QMatrix4x4 mRotation;
        /// View matrix, composed of translation and rotation.
        QMatrix4x4 mView;
        /// Projection matrix - perspective or orthographic.
        QMatrix4x4 mProjection;
        /// Composition of view and projection matrices.
        QMatrix4x4 mViewProjection;
    protected:
    }; // class SceneCamera
} // namespace Util

#endif // SCENE_CAMERA_H
