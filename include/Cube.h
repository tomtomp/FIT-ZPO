/**
 * @file Cube.h
 * @author Tomas Polasek
 * @brief Simple cube renderable in OpenGL.
 */

#ifndef CUBE_H
#define CUBE_H

#include "Types.h"
#include "Renderable.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Renderable cube.
    class Cube : public Renderable
    {
    public:
        /// Construct the Cube without the buffers.
        Cube();
        virtual ~Cube();

        /**
         * Create all necessary buffers and fill them.
         * The target context has to be already current.
         */
        virtual void create() override;

        /**
         * Render this object in current context.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void render() override;

        /**
         * Destroy OpenGL objects, they can be
         * re-created with create function.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void destroy() override;
    private:
        /// 6 faces, 2 triangles per.
        static constexpr unsigned int NUM_TRIANGLES{2 * 6};
        /// 3 vertices per triangle.
        static constexpr unsigned int NUM_VERTICES{NUM_TRIANGLES * 3};
        /// Position, Normal and UV.
        static constexpr unsigned int VALS_PER_VERTEX{3 + 3 + 2};

        /// Vertex data for Cube.
        static const GLfloat VERTEX_BUFFER_DATA[NUM_TRIANGLES * 3 * VALS_PER_VERTEX];

        /// The vertex array ID.
        GLuint mVa;
        /// The vertex buffer ID.
        GLuint mVb;
    protected:
    }; // class Cube
} // namespace Util

#endif //CUBE_H
