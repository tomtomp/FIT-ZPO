/**
 * @file SceneObject.h
 * @author Tomas Polasek
 * @brief Class used as a base for all object in the rendered scene.
 */

#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "Types.h"
#include "Renderable.h"
#include "ModelMatrix.h"

#include <QMatrix3x3>

/// Namespace containing utility functions and classes.
namespace Util
{
    class SceneObject : public Util::Renderable
    {
    public:
        /// Create object with default parameters.
        SceneObject() = default;
        explicit SceneObject(const SceneObject &other) :
            Renderable()
        { mModelMatrix = other.mModelMatrix; }
        virtual ~SceneObject() = default;

        /**
         * Set position of the object in scene world.
         * @param newPos New position of the object.
         * @warning Automatically recalculates the model matrix!
         */
        virtual void setPosition(const QVector3D &newPos)
        { mModelMatrix.setPosition(newPos); mModelMatrix.recalculate(); }

        /**
         * Set rotation of the object in scene world.
         * @param eulerAngles Euler angles used to rotate the object.
         * @warning Automatically recalculates the model matrix!
         */
        virtual void setEulerRotation(const QVector3D &eulerAngles)
        { mModelMatrix.setRotation(QQuaternion::fromEulerAngles(eulerAngles)); mModelMatrix.recalculate(); }

        /**
         * Set rotation of the object in scene world.
         * @param rotX Rotation around x-axis.
         * @param rotY Rotation around y-axis.
         * @param rotZ Rotation around z-axis.
         * @warning Automatically recalculates the model matrix!
         */
        virtual void setEulerRotation(float rotX, float rotY, float rotZ)
        { setEulerRotation({rotX, rotY, rotZ}); }

        /**
         * Set scale of the object in scene world.
         * @param newScale New scale of the object.
         * @warning Automatically recalculates the model matrix!
         */
        virtual void setScale(const QVector3D &newScale)
        { mModelMatrix.setScale(newScale); mModelMatrix.recalculate(); }

        /**
         * Set scale of the object in scene world.
         * @param scale Scale set for all axes.
         * @warning Automatically recalculates the model matrix!
         */
        virtual void setScale(float scale)
        { mModelMatrix.setScale(scale); mModelMatrix.recalculate(); }

        /// Get current position of the object.
        QVector3D position() const
        { return mModelMatrix.position(); }

        /// Get current rotation of the object.
        QVector3D eulerRotation() const
        { return mModelMatrix.eulerRotation(); }

        /// Get current scale of the object.
        QVector3D scale() const
        { return mModelMatrix.scale(); }

        /// Get the model matrix.
        const QMatrix4x4 modelMatrix() const
        { return mModelMatrix.model(); }

        /**
         * Get the transposed inversed model matrix.
         * @warning Presumes the matrix is orthogonal, i.e. inverse transpose
         * is the same as the original matrix.
         */
        const QMatrix3x3 modelInverseTransposeMatrix() const
        { return mModelMatrix.model().toGenericMatrix<3, 3>(); }
    private:
    protected:
        /// Model matrix containing position, rotation and scale of object.
        Util::ModelMatrix mModelMatrix;
    }; // class SceneObject
} // namespace Util

#endif // SCENEOBJECT_H
