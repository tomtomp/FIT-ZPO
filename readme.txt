        Změna obrazu přiložením čočky
          Tomáš Polášek (xpolas34)
        =============================

Tento projekt byl vytvořen do předmětu Zpracování Obrazu na Fakultě Informačních Technologií v Brně. Jeho cílem byla modifikace obrazu pomocí čoček, definovaných polygonální sítí a materiálových vlastností.

Obsah archivu
=============

 * src/, include/, resources/ a *.pro : Zdrojové soubory projektu.
 * readme.txt : Popis projektu.
 * models- : Adresář s testovacími modely.

Závislosti
==========

Pro překlad tohoto projektu jsou potřeba následující závislosti: 
 * Překladač jazyka C++, s podporou standardu C++14 (testováno na GCC 7.3.0)
 * Knihovny Qt, alespoň verze 5.x (testováno s Qt 5.10.1)

Překlad
=======

Překlad lze spustit následující sekvencí příkazů: 

> cd project_root
> mkdir build && cd build
> qmake ../
> make -j 4

Výsledný spustitelný soubor je potom uložen v adresáři "project_root/bin/LenseSimulator".

Alternativní způsob je stažení před připreveného archivu s přeloženým projektem a všemi závislostmi, který lze najít na adrese: 
> https://drive.google.com/file/d/16nQoXDMTR8FNUCbyRNSe5z7M32EpFCDc/edit
Tato distribuce byla testována na Windows 10 64bit.

Aplikace
========

Po jejím překladu, nebo po rozbalení před připraveného archivu lze aplikace spustit pomocí spustitelného souboru "LenseSimulator".

Pro běh aplikace je nutné, aby lokální prostředí podporovalo OpenGL alespoň ve verzi 4.5 Core.

Po spuštění aplikace se zobrazí hlavní okno, které je rozděleno do tří částí: 
 1. Menu lišta
 2. Zobrazovací plocha
 3. Záložky s nastavením

Pomocí menu lišty lze načítat vzorové obrazy a ukládat modifikované. Další možností je načtení výchozího testovacího vzoru.

Hlavní zobrazovací oblast umožňuje uživateli vidět aktuální rozložení scény. Pomocí tažení myši se zmáčknutým levým tlačítkem je také možné čočku otáčet a kolečkem na myši ji přibližovat/oddalovat.

Důležitou částí jsou také záložky s nastavením, které obsahují nastavení: 
 * Obrazu : Velikost, pozice a rotace
 * Kamery : Pozice a zorné pole
 * Čočky : Velikost, rotaci a pozici. Dále lze zde nastavit také index lomu, případnou chromatickou aberaci (pomocí Abbeho čísla) a tvar čočky (krychle, parametrizovatelná sférická čočka nebo model). Příklady modelů lze najít ve složce "models/"
 * Prostředí : Index lomu
 * Webkamery : Možnost získání obrazu z kamery, případně snímání videa.

