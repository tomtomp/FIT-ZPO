#version 450 core

noperspective in float fragDist;
noperspective in vec3 fragNorm;
noperspective in vec2 fragUv;

out vec4 outColor;

void main()
{
    // Write normal and distance to framebuffer.
    outColor = vec4(fragNorm, fragDist);
}
