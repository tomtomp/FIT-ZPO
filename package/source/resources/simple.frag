#version 450 core

#define M_PI 3.141592653589793238462643383
#define M_2PI 2.0 * 3.141592653589793238462643383

uniform sampler2D srcSampler;

in vec3 fragNorm;
in vec2 fragUv;

out vec4 outColor;

void main()
{
    outColor = texture2D(srcSampler, fragUv);
    //outColor = vec4(abs(fragNorm), 1.0f);
    //outColor = vec4(1.0f, 0.0f, 1.0f, 1.0f);
    /*
    outColor = vec4(1.0, 1.0, 1.0, 1.0);
    outColor = vec4(fragUv, length(fragUv), 1.0);
    outColor = vec4(fragNorm, 1.0);
    */
    /*
    float ux = float(viewWidth) / srcWidth - 0.5;
    float uy = float(viewHeight) / srcHeight - 0.5;
    vec2 ratioUv = fragUv * vec2(1.0 + 2.0 * ux, 1.0 + 2.0 * uy) - vec2(ux, uy);
    vec2 ratioUv = fragUv * (float(srcWidth) / srcHeight);
    outColor = texture2D(srcSampler, ratioUv);
    */
}
