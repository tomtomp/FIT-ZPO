#version 450 core

uniform sampler2D srcSampler;

in vec2 fragUv;

out vec4 outColor;

void main()
{
    /*
    vec4 texSample = texture2D(srcSampler, fragUv);
    vec3 normal = texSample.xyz;
    float dist = texSample.w / 3.0f;
    outColor = vec4(dist, dist, dist, 1.0f);
    */

    outColor = vec4(texture2D(srcSampler, fragUv).rgb, 1.0f);
}
