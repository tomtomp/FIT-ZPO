/**
 * @file Util.h
 * @author Tomas Polasek
 * @brief Utility function and classes.
 */

#include "Util.h"

namespace Util
{
    const char *glMessageTypeToStr(GLenum msgType)
    {
        switch (msgType)
        {
            case GL_DEBUG_TYPE_ERROR:
            {
                return "DEBUG_TYPE_ERROR";
            }
            case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            {
                return "DEBUG_TYPE_DEPRECATED_BEHAVIOR";
            }
            case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            {
                return "DEBUG_TYPE_UNDEFINED_BEHAVIOR";
            }
            case GL_DEBUG_TYPE_PORTABILITY:
            {
                return "DEBUG_TYPE_PORTABILITY";
            }
            case GL_DEBUG_TYPE_PERFORMANCE:
            {
                return "DEBUG_TYPE_PERFORMANCE";
            }
            case GL_DEBUG_TYPE_MARKER:
            {
                return "DEBUG_TYPE_MARKER";
            }
            case GL_DEBUG_TYPE_PUSH_GROUP:
            {
                return "DEBUG_TYPE_PUSH_GROUP";
            }
            case GL_DEBUG_TYPE_POP_GROUP:
            {
                return "DEBUG_TYPE_POP_GROUP";
            }
            case GL_DEBUG_TYPE_OTHER:
            {
                return "DEBUG_TYPE_OTHER";
            }
            default:
            {
                return "UNKNOWN";
            }
        }
    }

    const char *glMessageSeverityToStr(GLenum msgSeverity)
    {
        switch (msgSeverity)
        {
            case GL_DEBUG_SEVERITY_HIGH:
            {
                return "DEBUG_SEVERITY_HIGH";
            }
            case GL_DEBUG_SEVERITY_MEDIUM:
            {
                return "DEBUG_SEVERITY_MEDIUM";
            }
            case GL_DEBUG_SEVERITY_LOW:
            {
                return "DEBUG_SEVERITY_LOW";
            }
            case GL_DEBUG_SEVERITY_NOTIFICATION:
            {
                return "DEBUG_SEVERITY_NOTIFICATION";
            }
            default:
            {
                return "UNKNOWN";
            }
        }
    }

    double distance(double x1, double y1, double x2, double y2)
    {
        double distX{x2 - x1};
        double distY{y2 - y1};
        return sqrt(distX * distX + distY * distY);
    }

}
