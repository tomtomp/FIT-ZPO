/**
 * @file Mouse.cpp
 * @author Tomas Polasek
 * @brief Mouse handling class.
 */

#include "Mouse.h"

namespace Util
{
    void Mouse::setDragAction(Qt::MouseButton key, Qt::KeyboardModifiers mods)
    {
        if (mCurrentDrag.dragging && mCurrentDrag.keyCombination == KeyCombination{key, mods, ACTION_DRAG})
        { // If the drag is currently started.
            if (mCurrentDrag.started)
            { // We should send end event.
                mCurrentDrag.dragFun.end(mCurrentDrag.startX, mCurrentDrag.startY, mCurrentDrag.startX, mCurrentDrag.startY);
                mCurrentDrag.started = false;
            }
            mCurrentDrag.dragging = false;
        }
    }

    void Mouse::dispatchMouse(Qt::MouseButton key, Qt::KeyboardModifiers mods, Util::Mouse::Action action, double x, double y)
    {
        decltype(mMapping.begin()) search{mMapping.find({key, mods, action})};

        if (search != mMapping.end())
        { // We found an action!
            search->second(x, y);
        }
        else if (mDefaultAction)
        {
            mDefaultAction(key, mods, action, x, y);
        }

        processDragButton(key, mods, action, x, y);
    }

    void Mouse::dispatchScroll(Qt::KeyboardModifiers mods, double xOffset, double yOffset, double x, double y)
    {
        if (mScrollFun)
        {
            mScrollFun(mods, xOffset, yOffset, x, y);
        }
    }

    void Mouse::dispatchPosition(double x, double y)
    {
        if (mMousePosFun)
        {
            mMousePosFun(x, y);
        }

        processDragPosition(x, y);
    }

    void Mouse::debugPrintButtonAction(Qt::MouseButton button, Qt::KeyboardModifiers mods, Util::Mouse::Action action, double x, double y)
    {
        qDebug() << "Key: " << QKeySequence(button).toString()
                 << "; Mods: " << QKeySequence(mods).toString()
                 << "; Action: " << actionToStr(action)
                 << "; x: " << x << "; y: " << y;
    }

    const char *Mouse::actionToStr(Util::Mouse::Action action)
    {
        switch (action)
        {
            case ACTION_PRESS:
            {
                return "Press";
            }
            case ACTION_RELEASE:
            {
                return "Release";
            }
            default:
            {
                break;
            }
        }
        return "Unknown";
    }

    void Mouse::processDragButton(Qt::MouseButton button, Qt::KeyboardModifiers mods, Action action, double x, double y)
    {
        decltype(mDragMapping.begin()) dragSearch{mDragMapping.find({button, mods, ACTION_DRAG})};

        if (dragSearch != mDragMapping.end())
        { // We found a drag action!
            if (action == ACTION_PRESS && !mCurrentDrag.dragging)
            { // Possibly a new drag starting.
                mCurrentDrag.dragFun = dragSearch->second;
                mCurrentDrag.dragging = true;
                mCurrentDrag.keyCombination = {button, mods, ACTION_DRAG};
                mCurrentDrag.startX = x;
                mCurrentDrag.startY = y;
                mCurrentDrag.started = false;
            }
            else if (action == ACTION_RELEASE && mCurrentDrag.dragging)
            { // Possibly a drag ending.
                if (mCurrentDrag.keyCombination == KeyCombination{button, mods, ACTION_DRAG})
                { // End the drag.
                    if (mCurrentDrag.started)
                    {
                        mCurrentDrag.dragFun.end(mCurrentDrag.startX, mCurrentDrag.startY, x, y);
                    }
                    mCurrentDrag.dragging = false;
                }
            }
        }
    }

    void Mouse::processDragPosition(double x, double y)
    {
        if (mCurrentDrag.dragging)
        {
            if (mCurrentDrag.started)
            { // Already started -> send the drag event.
                mCurrentDrag.dragFun.drag(mCurrentDrag.startX, mCurrentDrag.startY, x, y);
            }
            else if (Util::distance(mCurrentDrag.startX, mCurrentDrag.startY, x, y) > mCurrentDrag.dragFun.deadZone)
            { // We should send start event.
                mCurrentDrag.dragFun.start(x, y);
                mCurrentDrag.started = true;
            }
        }
    }
}
