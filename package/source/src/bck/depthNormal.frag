#version 450 core

in float fragDist;
in vec3 fragNorm;
in vec2 fragUv;

out vec4 outColor;

void main()
{
    // Write normal and distance to framebuffer.
    outColor = vec4(fragNorm, fragDist);
}
