#version 450 core

uniform mat4x4 mvp;

layout(location = 0) in vec3 vertPos;
layout(location = 1) in vec3 vertNorm;
layout(location = 2) in vec2 vertUv;

out vec2 fragUv;

void main()
{
    fragUv = vertUv;

    gl_Position = mvp * vec4(vertPos, 1.0);
}
