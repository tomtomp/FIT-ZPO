/**
 * @file RenderArea.h
 * @author Tomas Polasek
 * @brief Class representing the OpenGL draw area.
 */

#include "RenderArea.h"

RenderArea::RenderArea(QWidget *parent) :
    QOpenGLWidget(parent)
{
    setupContext();
    setupTimer();

    setFocusPolicy(Qt::StrongFocus);
}

RenderArea::~RenderArea()
{
    if (mEngine)
    {
        makeCurrent();
        delete mEngine;
        mEngine = nullptr;
    }
}

void RenderArea::startRenderer()
{
    mUpdateLagMs = 0.0f;
    mFrameLagMs = 0.0f;
    mFrameCounter = 0u;
    mUpdateCounter = 0u;

    mFrameTime = 0.0f;
    mFps = 0.0f;
    mUps = 0.0f;

    mCounterTimer.reset();
    mElapsedTimer.reset();

    mRenderTimer->start(DEFAULT_TIMER_MSEC);

    mPresentingVideo = false;
}

void RenderArea::stopRenderer()
{
    mRenderTimer->stop();
}

void RenderArea::initializeGL()
{
    qDebug() << "RenderArea OpenGL initialization...";

    Util::gl.initializeOpenGLFunctions();

#ifdef QT_DEBUG
    Util::gl.glEnable(GL_DEBUG_OUTPUT);
    Util::gl.glDebugMessageCallback(reinterpret_cast<GLDEBUGPROC>(RenderArea::glDebugCallbackFun), this);
#endif

    Util::gl.glEnable(GL_DEPTH_TEST);

    // Without parent pointer, so we can free OpenGL resources at correct time.
    mEngine = new Engine::LenseEngine(width(), height());
    //mEngine->setViewSize(width(), height());

    // Notify that engine has been initialized.
    emit engineInitialized();
}

void RenderArea::resizeGL(int width, int height)
{
    qDebug() << "RenderArea changed size to : " << width << "x" << height;
    mEngine->setViewSize(width, height);
}

void RenderArea::paintGL()
{
    render();
}

void RenderArea::setupContext()
{
    QSurfaceFormat glFormat;
    glFormat.setVersion(DEFAULT_GL_VERSION_MAJOR, DEFAULT_GL_VERSION_MINOR);
    glFormat.setProfile(DEFAULT_GL_PROFILE);
    glFormat.setSwapBehavior(DEFAULT_GL_SWAP_BEHAVIOR);
    glFormat.setSwapInterval(DEFAULT_GL_SWAP_INTERVAL);
    glFormat.setSamples(DEFAULT_GL_SAMPLES);
    setFormat(glFormat);

    mTargetUT = MS_PER_S / DEFAULT_UPS;
    mTargetFT = MS_PER_S / DEFAULT_FPS;
}

void RenderArea::setupTimer()
{
    mRenderTimer = new QTimer(this);
    connect(mRenderTimer, &QTimer::timeout, this, &RenderArea::updateState);
}

void RenderArea::updateState()
{
    float newLag{mElapsedTimer.elapsedReset<Util::Timer::microseconds>() / US_PER_MS};

    mUpdateLagMs += newLag;
    mFrameLagMs += newLag;

    uint64_t recalcTime{mCounterTimer.elapsed<Util::Timer::milliseconds>()};
    if (recalcTime > TIME_PER_RECALC)
    {
        qDebug() << "Frames: " << mFrameCounter << " ; Updates: " << mUpdateCounter;
        // Calculate time since last calculation in seconds.
        float sinceRecalc{recalcTime / MS_PER_S};
        // Calculate FPS, frame time and UPS.
        mFps = mFrameCounter / sinceRecalc;
        mFrameTime = static_cast<float>(recalcTime) / mFrameCounter;
        mFrameCounter = 0u;
        mUps = mUpdateCounter / sinceRecalc;
        mUpdateCounter = 0u;

        // Restart the timer.
        mCounterTimer.reset();
    }

    while (mUpdateLagMs >= mTargetUT)
    { // Catch up with required updates.
        mEngine->update(mTargetUT);
        mUpdateCounter++;
        mUpdateLagMs -= mTargetUT;
    }

    if (mFrameLagMs >= mTargetFT)
    {
        // Calling update triggers re-draw.
        update();
        // Increment mFrameCounter in the redraw function.

        // Reset frame lag, skipping any frames which could not be rendered in time.
        mFrameLagMs = 0.0f;
    }
}

void RenderArea::renderOverlay()
{
    /*
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QString frameTimeStr = QString("Frametime: %1 ms").arg(QString::number(mFrameTime, 'g', 1));
    QString fpsStr = QString("FPS : %1").arg(QString::number(mFps, 'g', 1));
    QString upsStr = QString("UPS : %1").arg(QString::number(mUps, 'g', 1));

    // FrameTime string is the longest one...
    int textWidth{painter.fontMetrics().width(frameTimeStr)};
    int textHeight{painter.fontMetrics().height()};

    QPainterPath backgroundPath;
    backgroundPath.addRoundedRect(0, 0, 3 * textHeight, textWidth, 20.0, 15.0);
    QPen pen(Qt::black, 10);
    painter.setPen(pen);
    painter.fillPath(backgroundPath, Qt::red);
    painter.drawPath(backgroundPath);

    // Black text color.
    painter.setPen(QColor(0, 0, 0));
    painter.end();
    */
}

#ifdef QT_DEBUG
void RenderArea::glDebugCallbackFun(GLenum source, GLenum type, GLuint id,
                                    GLenum severity, GLsizei length,
                                    const GLchar *message, const void *userParam)
{
    Q_UNUSED(source);
    Q_UNUSED(id);
    Q_UNUSED(length);
    Q_UNUSED(userParam);

    QString displayMsg = QString("GL CALLBACK: type = %1, severity = %2, message = %3")
            .arg(Util::glMessageTypeToStr(type),
                 Util::glMessageSeverityToStr(severity),
                 message);

    qDebug() << displayMsg;
}
#endif

void RenderArea::setDefaultImage()
{
    mEngine->setDefaultSourceImage();
    mPresentingVideo = false;
}

void RenderArea::setImage(QImage &&image)
{
    // Force re-creation of texture.
    mEngine->setSourceImage(std::move(image), true);
    mPresentingVideo = false;
}

void RenderArea::setVideoFrame(QImage &&frame, bool formatChanged)
{
    // Re-create texture only if format changed, or we switched to video.
    mEngine->setSourceImage(std::move(frame), formatChanged || !mPresentingVideo);
    mPresentingVideo = true;
}

void RenderArea::keyPressEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat())
    {
        mEngine->dispatchKeyboard(static_cast<Qt::Key>(event->key()), event->modifiers(), Util::Keyboard::ACTION_REPEAT);
    }
    else
    {
        mEngine->dispatchKeyboard(static_cast<Qt::Key>(event->key()), event->modifiers(), Util::Keyboard::ACTION_PRESS);
    }
}

void RenderArea::keyReleaseEvent(QKeyEvent *event)
{
    if (!event->isAutoRepeat())
    {
        mEngine->dispatchKeyboard(static_cast<Qt::Key>(event->key()), event->modifiers(), Util::Keyboard::ACTION_RELEASE);
    }
}

void RenderArea::mouseMoveEvent(QMouseEvent *event)
{
    mEngine->dispatchPosition(event->x(), event->y());
}

void RenderArea::mousePressEvent(QMouseEvent *event)
{
    const QPointF &pos = event->localPos();
    mEngine->dispatchMouse(event->button(), event->modifiers(), Util::Mouse::ACTION_PRESS, pos.x(), pos.y());
}

void RenderArea::mouseReleaseEvent(QMouseEvent *event)
{
    const QPointF &pos = event->localPos();
    mEngine->dispatchMouse(event->button(), event->modifiers(), Util::Mouse::ACTION_RELEASE, pos.x(), pos.y());
}

void RenderArea::wheelEvent(QWheelEvent *event)
{
    QPoint delta = event->angleDelta();
    const QPointF &pos = event->posF();
    mEngine->dispatchScroll(event->modifiers(), delta.x(), delta.y(), pos.x(), pos.y());
}

void RenderArea::render()
{
    mEngine->render();
    //renderOverlay();
    mFrameCounter++;
}
