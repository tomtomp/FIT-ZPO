/**
 * @file OpenGL.cpp
 * @author Tomas Polasek
 * @brief Globally accesible object containing OpenGL functions.
 */

#include "OpenGL.h"

namespace Util
{
    // Definition of the globally available repository of OpenGL functions.
    QOpenGLFunctions_4_5_Core gl;
}
