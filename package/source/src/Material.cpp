/**
 * @file Material.cpp
 * @author Tomas Polasek
 * @brief Class used for description of lense materials.
 */

#include "Material.h"

namespace Util
{
    void Material::setBaseRefractiveIndex(float newIndex)
    {
        mBaseRefractiveIndex = newIndex;
        recalculateAberration();
    }

    void Material::setAbbeNumber(float newAbbe)
    {
        mAbbeNumber = newAbbe;
        recalculateAberration();
    }

    void Material::setChromaticAberration(bool enabled)
    {
        mChromaticAberration = enabled;
        recalculateAberration();
    }

    void Material::recalculateAberration()
    {
        if (mChromaticAberration)
        {
            /*
             * Base formula:
             *      n_d - 1
             * V = ---------
             *     n_f - n_c
             *                      n_d - 1
             * change = n_f - n_c = -------
             *                         V
             */

            float normalizedChange{((mBaseRefractiveIndex - 1.0f) / mAbbeNumber) / (BLUE_CHANGE + RED_CHANGE)};

            // Refractive index for red:
            mAberratedIndices.setX(mBaseRefractiveIndex - normalizedChange * RED_CHANGE);
            // Refractive index for green:
            mAberratedIndices.setY(mBaseRefractiveIndex);
            // Refractive index for blue:
            mAberratedIndices.setZ(mBaseRefractiveIndex - normalizedChange * BLUE_CHANGE);

            qDebug() << "Chromatic refraction indices [R, G, B]: " << mAberratedIndices.x() << " " << mAberratedIndices.y() << " " << mAberratedIndices.z();
        }

    }
}
