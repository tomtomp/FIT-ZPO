/**
 * @file LenseEngine.h
 * @author Tomas Polasek
 * @brief Engine containing the scene and lense settings.
 */

#include "LenseEngine.h"

namespace Engine
{
    // For simplicity...
    using namespace Util;

    LenseEngine::LenseEngine(int width, int height, QObject *parent):
        QObject(parent)
    {
        initGl();
        initBindings();
        initCamera();
        setViewSize(width, height);
    }

    LenseEngine::~LenseEngine()
    {
    }

    void LenseEngine::update(float deltaT)
    {
        Q_UNUSED(deltaT);
        /*
        mLense->setEulerRotation((mBaseRotation *
                                  QQuaternion::fromEulerAngles(mRequestRotation.y(), mRequestRotation.x(), 0.0f)
                                  ).toEulerAngles());
                                  */
        // TODO - Finish.
        //QQuaternion q = QQuaternion::fromEulerAngles(mRequestRotation.y(), mRequestRotation.x(), 0.0f) * mBaseRotation;
        /*
        mLense->setEulerRotation(mBaseRotation.toEulerAngles() +
                                 QVector3D(mRequestRotation.y(), mRequestRotation.x(), 0.0f));
                                 */
        /*
        mLense->setEulerRotation(mBaseRotation.toEulerAngles() +
                                 q.toEulerAngles());
                                 */
        //mLense->setEulerRotation(mRequestRotation.y(), mRequestRotation.x(), 0.0f);

        if (mChangingRotation)
        {
            mLense->setEulerRotation((QQuaternion::fromEulerAngles(mRequestRotation.y(), mRequestRotation.x(), 0.0f) * mBaseRotation).toEulerAngles());
            emit rotationChanged(mLense->eulerRotation());
        }
        if (abs(mPosChange) > 0.001f)
        {
            mLense->setPosition(QVector3D(0.0f, 0.0f, mLense->position().z() - mPosChange));
            mPosChange = 0.0f;
            emit positionChanged(mLense->position());
        }
    }

    void LenseEngine::setViewSize(int width, int height)
    {
        mViewportWidth = width;
        mViewportHeight = height;

        refreshCamera();
    }

    void LenseEngine::render()
    {
        if (!mLense->created())
        {
            mLense->create();
        }

        mCamera.recalculate();

        if (mViewportWidth != mFb->width() || mViewportHeight != mFb->height())
        {
            mFb.reset(new QOpenGLFramebufferObject(mViewportWidth, mViewportHeight, QOpenGLFramebufferObject::Depth, GL_TEXTURE_2D, GL_RGBA32F));
            mFb->addColorAttachment({mViewportWidth, mViewportHeight});
        }
        if (mViewportWidth != mSceneFb->width() || mViewportHeight != mSceneFb->height())
        {
            mSceneFb.reset(new QOpenGLFramebufferObject(mViewportWidth, mViewportHeight, QOpenGLFramebufferObject::Depth, GL_TEXTURE_2D, GL_RGBA));
            mSceneFb->addColorAttachment({mViewportWidth, mViewportHeight});
        }

        const QMatrix4x4 vp = mCamera.vp();

        mFb->bind();
        gl.glClearDepth(0.0);
        gl.glDepthFunc(GL_GEQUAL);
        gl.glCullFace(GL_BACK);
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        mDepthNormProg->bind();
        { // Depth shader.
            {
                const QMatrix4x4 mv = mCamera.v() * mLense->modelMatrix();
                const QMatrix4x4 mvp = vp * mLense->modelMatrix();
                const QMatrix3x3 invmt = mLense->modelInverseTransposeMatrix();

                mDepthNormProg->setUniformValue(mUl.depthNormMv, mv);
                mDepthNormProg->setUniformValue(mUl.depthNormMvp, mvp);
                mDepthNormProg->setUniformValue(mUl.depthNormInvmt, invmt);
                mLense->render();
            }
        } // Depth shader.
        mDepthNormProg->release();
        mFb->release();

        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        gl.glClearDepth(1.0);
        gl.glDepthFunc(GL_LESS);
        gl.glCullFace(GL_FRONT);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        /*
        mSceneFb->bind();
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        mImageProg->bind();
        { // Image shader.
            {
                const QMatrix4x4 mvp = vp * mSrcImage->modelMatrix();
                mImageProg->setUniformValue(mUl.imageSrcSampler, 0);
                mImageProg->setUniformValue(mUl.imageMvp, mvp);
                mSrcImage->render();
            }
        } // Image shader.
        mImageProg->release();
        mSceneFb->release();
        */

        if (mWireFrame)
        {
            gl.glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }

        if (mLenseMaterial.chromaticAberration())
        {
            mLenseChromaProg->bind();
            { // Lense shader.
                {
                    const QMatrix4x4 mv = mCamera.v() * mLense->modelMatrix();
                    const QMatrix4x4 mvp = vp * mLense->modelMatrix();
                    const QMatrix3x3 invmt = mLense->modelInverseTransposeMatrix();
                    const QMatrix4x4 bgMvInv = (mCamera.v() * mSrcImage->modelMatrix()).inverted();

                    mLenseChromaProg->setUniformValue(mUl.lenseCMode, mMode);
                    mLenseChromaProg->setUniformValue(mUl.lenseCMap, 0);
                    mLenseChromaProg->setUniformValue(mUl.lenseCBackground, 1);
                    mLenseChromaProg->setUniformValue(mUl.lenseCMv, mv);
                    mLenseChromaProg->setUniformValue(mUl.lenseCMvp, mvp);
                    mLenseChromaProg->setUniformValue(mUl.lenseCInvmt, invmt);
                    mLenseChromaProg->setUniformValue(mUl.lenseCP, mCamera.p());
                    mLenseChromaProg->setUniformValue(mUl.lenseCEnvRI, mEnvMaterial.baseRefractiveIndex());
                    mLenseChromaProg->setUniformValue(mUl.lenseCRI, mLenseMaterial.aberratedIndices());
                    mLenseChromaProg->setUniformValue(mUl.lenseCBgMvInv, bgMvInv);

                    gl.glActiveTexture(GL_TEXTURE0);
                    gl.glBindTexture(GL_TEXTURE_2D, mFb->texture());
                    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

                    gl.glActiveTexture(GL_TEXTURE1);
                    gl.glBindTexture(GL_TEXTURE_2D, mSrcImage->textureId());
                    mLense->render();
                }
            } // Lense shader.
            mLenseChromaProg->release();
        }
        else
        {
            mLenseProg->bind();
            { // Lense shader.
                {
                    const QMatrix4x4 mv = mCamera.v() * mLense->modelMatrix();
                    const QMatrix4x4 mvp = vp * mLense->modelMatrix();
                    const QMatrix3x3 invmt = mLense->modelInverseTransposeMatrix();
                    const QMatrix4x4 bgMvInv = (mCamera.v() * mSrcImage->modelMatrix()).inverted();

                    mLenseProg->setUniformValue(mUl.lenseMode, mMode);
                    mLenseProg->setUniformValue(mUl.lenseMap, 0);
                    mLenseProg->setUniformValue(mUl.lenseBackground, 1);
                    mLenseProg->setUniformValue(mUl.lenseMv, mv);
                    mLenseProg->setUniformValue(mUl.lenseMvp, mvp);
                    mLenseProg->setUniformValue(mUl.lenseInvmt, invmt);
                    mLenseProg->setUniformValue(mUl.lenseP, mCamera.p());
                    mLenseProg->setUniformValue(mUl.lenseEnvRI, mEnvMaterial.baseRefractiveIndex());
                    mLenseProg->setUniformValue(mUl.lenseRI, mLenseMaterial.baseRefractiveIndex());
                    mLenseProg->setUniformValue(mUl.lenseBgMvInv, bgMvInv);

                    gl.glActiveTexture(GL_TEXTURE0);
                    gl.glBindTexture(GL_TEXTURE_2D, mFb->texture());
                    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

                    gl.glActiveTexture(GL_TEXTURE1);
                    gl.glBindTexture(GL_TEXTURE_2D, mSrcImage->textureId());
                    mLense->render();
                }
            } // Lense shader.
            mLenseProg->release();
        }

        if (mWireFrame)
        {
            gl.glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        mImageProg->bind();
        { // Image shader.
            {
                const QMatrix4x4 mvp = vp * mSrcImage->modelMatrix();
                mImageProg->setUniformValue(mUl.imageSrcSampler, 0);
                mImageProg->setUniformValue(mUl.imageMvp, mvp);
                mSrcImage->render();
            }
        } // Image shader.
        mImageProg->release();

#if 0
        /*
        if (mViewportWidth != mFb->width() || mViewportHeight != mFb->height())
        {
            mFb.reset(new QOpenGLFramebufferObject(mViewportWidth, mViewportHeight, QOpenGLFramebufferObject::Depth));
            mFb->addColorAttachment({mViewportWidth, mViewportHeight});
            //mFb.reset(new QOpenGLFramebufferObject(mViewportWidth, mViewportHeight));
        }
        */

        mProgram->bind();
        {
            const QMatrix4x4 mvp = vp * mLense->modelMatrix();
            const QMatrix3x3 invmt = mLense->modelInverseTransposeMatrix();
            mProgram->setUniformValue(mUl.srcSampler, 0);
            mProgram->setUniformValue(mUl.mvp, mvp);
            mProgram->setUniformValue(mUl.invmt, invmt);
            mSrcImage->bind();
            mLense->render();

            /*
            mFb->bind();
            gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            {
                const QMatrix4x4 mvp = vp * mSrcImage->modelMatrix();
                const QMatrix3x3 invmt = mSrcImage->modelInverseTransposeMatrix();
                mProgram->setUniformValue(mUl.srcSampler, 0);
                mProgram->setUniformValue(mUl.mvp, mvp);
                mProgram->setUniformValue(mUl.invmt, invmt);
                mSrcImage->render();
            }

            {
                const QMatrix4x4 mvp = vp * mLense->modelMatrix();
                const QMatrix3x3 invmt = mLense->modelInverseTransposeMatrix();
                mProgram->setUniformValue(mUl.srcSampler, 0);
                mProgram->setUniformValue(mUl.mvp, mvp);
                mProgram->setUniformValue(mUl.invmt, invmt);
                mSrcImage->bind();
                mLense->render();
                mSrcImage->release();
            }
            mFb->release();

            {
                const QMatrix4x4 mvp = vp * mLense->modelMatrix();
                const QMatrix3x3 invmt = mLense->modelInverseTransposeMatrix();
                mProgram->setUniformValue(mUl.srcSampler, 0);
                mProgram->setUniformValue(mUl.mvp, mvp);
                mProgram->setUniformValue(mUl.invmt, invmt);
                gl.glActiveTexture(GL_TEXTURE0);
                gl.glBindTexture(GL_TEXTURE_2D, mFb->texture());
                mLense->render();
                gl.glBindTexture(GL_TEXTURE_2D, 0);
            }
            */
        }
        mProgram->release();
#endif
    }

    void LenseEngine::setDefaultSourceImage()
    {
        mSrcImage->setDefaultImage();
    }

    void LenseEngine::setSourceImage(QImage &&image, bool destroy)
    {
        mSrcImage->setImage(std::move(image), destroy);
    }

    void LenseEngine::setCameraProjection(float fov)
    {
        mProjectionPerspective = true;
        mProjectionFOV = fov;

        refreshCamera();
    }

    void LenseEngine::setCameraOrthographic()
    {
        mProjectionPerspective = false;

        refreshCamera();
    }

    void LenseEngine::setLenseChromaticAberration(bool enabled, float abbe)
    {
        mLenseMaterial.setAbbeNumber(abbe);
        mLenseMaterial.setChromaticAberration(enabled);
    }

    void LenseEngine::setLenseTypeCube()
    {
        if (mLense)
        {
            mLense.reset(new Util::DummyLense(*mLense));
        }
        else
        {
            mLense.reset(new Util::DummyLense());
        }
    }

    void LenseEngine::setLenseTypeSphere(float foc1, float radius1, float foc2, float radius2, float thickness, std::size_t rings, std::size_t sectors)
    {
        if (mLense)
        {
            mLense.reset(new Util::SphericalLense(*mLense, foc1, radius1, foc2, radius2, thickness, rings, sectors));
        }
        else
        {
            mLense.reset(new Util::SphericalLense(foc1, radius1, foc2, radius2, thickness, rings, sectors));
        }
    }

    void LenseEngine::setLenseModel(QFile &input)
    {
        Util::Lense *newLense{nullptr};
        try {
            if (mLense)
            {
                newLense = new Util::ModelLense(*mLense, input);
            }
            else
            {
                newLense = new Util::ModelLense(input);
            }
        } catch (...) {
            throw;
        }
        mLense.reset(newLense);
    }

    void LenseEngine::initGl()
    {
        // TODO - Create wrapper.
        mImageProg = new QOpenGLShaderProgram(this);
        mImageProg->create();
        mImageProg->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/srcImage.vert");
        mImageProg->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/srcImage.frag");
        mImageProg->link();
        mUl.checkSet(mUl.imageSrcSampler, mImageProg, "srcSampler");
        mUl.checkSet(mUl.imageMvp, mImageProg, "mvp");

        // TODO - Create wrapper.
        mLenseProg = new QOpenGLShaderProgram(this);
        mLenseProg->create();
        mLenseProg->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/lense.vert");
        mLenseProg->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/lense.frag");
        mLenseProg->link();
        mUl.checkSet(mUl.lenseMode, mLenseProg, "mode");
        mUl.checkSet(mUl.lenseMap, mLenseProg, "depthNormalMap");
        mUl.checkSet(mUl.lenseBackground, mLenseProg, "background");
        mUl.checkSet(mUl.lenseMv, mLenseProg, "mv");
        mUl.checkSet(mUl.lenseMvp, mLenseProg, "mvp");
        mUl.checkSet(mUl.lenseInvmt, mLenseProg, "invmt");
        mUl.checkSet(mUl.lenseP, mLenseProg, "p");
        mUl.checkSet(mUl.lenseEnvRI, mLenseProg, "envRI");
        mUl.checkSet(mUl.lenseRI, mLenseProg, "lRI");
        mUl.checkSet(mUl.lenseBgMvInv, mLenseProg, "bgMvInv");

        // TODO - Create wrapper.
        mLenseChromaProg = new QOpenGLShaderProgram(this);
        mLenseChromaProg->create();
        mLenseChromaProg->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/lense.vert");
        mLenseChromaProg->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/lenseChroma.frag");
        mLenseChromaProg->link();
        mUl.checkSet(mUl.lenseCMode, mLenseProg, "mode");
        mUl.checkSet(mUl.lenseCMap, mLenseProg, "depthNormalMap");
        mUl.checkSet(mUl.lenseCBackground, mLenseProg, "background");
        mUl.checkSet(mUl.lenseCMv, mLenseProg, "mv");
        mUl.checkSet(mUl.lenseCMvp, mLenseProg, "mvp");
        mUl.checkSet(mUl.lenseCInvmt, mLenseProg, "invmt");
        mUl.checkSet(mUl.lenseCP, mLenseProg, "p");
        mUl.checkSet(mUl.lenseCEnvRI, mLenseProg, "envRI");
        mUl.checkSet(mUl.lenseCRI, mLenseProg, "lRI");
        mUl.checkSet(mUl.lenseCBgMvInv, mLenseProg, "bgMvInv");

        // TODO - Create wrapper.
        mDepthNormProg = new QOpenGLShaderProgram(this);
        mDepthNormProg->create();
        mDepthNormProg->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/depthNormal.vert");
        mDepthNormProg->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/depthNormal.frag");
        mDepthNormProg->link();
        mUl.checkSet(mUl.depthNormMv, mDepthNormProg, "mv");
        mUl.checkSet(mUl.depthNormMvp, mDepthNormProg, "mvp");
        mUl.checkSet(mUl.depthNormInvmt, mDepthNormProg, "invmt");

        mFb.reset(new QOpenGLFramebufferObject(1, 1, QOpenGLFramebufferObject::Depth));
        mSceneFb.reset(new QOpenGLFramebufferObject(1, 1, QOpenGLFramebufferObject::Depth));

        mSrcImage.reset(new Util::ImageSrc(true));
        mSrcImage->create();
        mSrcImage->setTextureUnit(0u);

        mLense.reset(new Util::DummyLense());
        //mLense.reset(new Util::SphericalLense());
        mLense->create();
        mLense->setScale(Config::LENSE_SIZE);

        gl.glClearColor(0.0, 0.0, 0.0, 1.0);

        mRequestRotation = QVector2D(0.0f, 0.0f);
        mChangingRotation = false;
        mPosChange = 0.0f;
        mMode = 0;
        mWireFrame = false;
        mLenseMaterial.setBaseRefractiveIndex(Config::LENSE_REFR_INDEX);
        mLenseMaterial.setAbbeNumber(Config::LENSE_ABBE_NUMBER);
        mEnvMaterial.setBaseRefractiveIndex(Config::ENV_REFR_INDEX);
    }

    void LenseEngine::initBindings()
    {
#if 0
        // Debug information about user inputs:
        mKeyboard.setDefaultAction([] (Qt::Key key, Qt::KeyboardModifiers mods, Util::Keyboard::Action action) {
            Util::Keyboard::debugPrintKeyAction(key, mods, action);
        });
        mMouse.setDefaultAction([] (Qt::MouseButton button, Qt::KeyboardModifiers mods, Util::Mouse::Action action, double x, double y) {
            Util::Mouse::debugPrintButtonAction(button, mods, action, x, y);
        });
        mMouse.setMousePosAction([] (double x, double y) {
            qDebug() << "Mouse position x: " << x << "; y: " << y;
        });
        mMouse.setScrollAction([] (Qt::KeyboardModifiers mods, double xOffset, double yOffset, double x, double y) {
            qDebug() << "Mouse scroll mods: " << QKeySequence(mods).toString()
                     << "; xOff: " << xOffset << "; yOff: " << yOffset
                     << "; x: " << x << "; y: " << y;
        });
        mMouse.setDragAction(Qt::LeftButton, Qt::NoModifier, 1.0f,
                             [] (double x, double y)
        {
            qDebug() << "Starting drag at: " << x << " x " << y;
        }, [] (double startX, double startY, double x, double y)
        {
            qDebug() << "Dragging at: " << x << " x " << y << " started at: " << startX << " x " << startY;
        }, [] (double startX, double startY, double x, double y)
        {
            qDebug() << "Ending drag at: " << x << " x " << y << " started at: " << startX << " x " << startY;
        });
#endif
        mMouse.setDragAction(Qt::LeftButton, Qt::NoModifier, 1.0f,
                             [this] (double x, double y)
        {
            mChangingRotation = true;
            mBaseRotation = QQuaternion::fromEulerAngles(mLense->eulerRotation());
            mRequestRotation = QVector2D(0.0f, 0.0f);
            Q_UNUSED(x);
            Q_UNUSED(y);
        }, [this] (double startX, double startY, double x, double y)
        {
            mRequestRotation = QVector2D(x - startX, y - startY);
        }, [this] (double startX, double startY, double x, double y)
        {
            mChangingRotation = false;
            Q_UNUSED(startX);
            Q_UNUSED(startY);
            Q_UNUSED(x);
            Q_UNUSED(y);
        });

        mMouse.setScrollAction([this] (Qt::KeyboardModifiers, double xOffset, double yOffset, double, double) {
            Q_UNUSED(xOffset);
            mPosChange += yOffset / 180.0f;
        });

        mKeyboard.setAction(Qt::Key_Space, Qt::NoModifier, Keyboard::ACTION_PRESS, [this] () {
            ++mMode;
            mMode = mMode % NUM_MODES;
        });

        mKeyboard.setAction(Qt::Key_W, Qt::NoModifier, Keyboard::ACTION_PRESS, [this] () {
            mWireFrame = !mWireFrame;
        });
    }

    void LenseEngine::initCamera()
    {
        mCamera.setPos(Config::CAMERA_POS_X, Config::CAMERA_POS_Y, Config::CAMERA_POS_Z);
        // No rotation.
        mProjectionPerspective = Config::CAMERA_PERSPECTIVE;
        mProjectionFOV = Config::CAMERA_FOV;
    }

    void LenseEngine::refreshCamera()
    {
        if (mProjectionPerspective)
        {
            double aspectRatio{static_cast<double>(mViewportWidth) / mViewportHeight};
            mCamera.setPerspective(mProjectionFOV,
                                   aspectRatio,
                                   Config::CAMERA_NEAR,
                                   Config::CAMERA_FAR);
        }
        else
        {
            double halfX{mViewportWidth / 2.0 / Config::CAMERA_ORHTO_DIV};
            double halfY{mViewportHeight / 2.0 / Config::CAMERA_ORHTO_DIV};
            mCamera.setOrtho(-halfX, halfX,
                             -halfY, halfY,
                             Config::CAMERA_NEAR,
                             Config::CAMERA_FAR);
        }
        mCamera.recalculate();
    }

    /*
    void LenseEngine::calculateSourceTransform()
    {
        float ux{(static_cast<float>(mViewportWidth) / mSrcImage->width() - 1.0f) / 2.0f};
        float uy{(static_cast<float>(mViewportHeight) / mSrcImage->height() - 1.0f) / 2.0f};

        mSourceImageTransform.setToIdentity();
        mSourceImageTransform(0, 0) = 1 + 2.0f * ux;
        mSourceImageTransform(0, 2) = -ux;
        mSourceImageTransform(1, 1) = 1 + 2.0f * uy;
        mSourceImageTransform(1, 2) = -uy;

        qDebug() << mSourceImageTransform;
    }
    */

    bool LenseEngine::UniformLocations::checkSet(GLint &target, QOpenGLShaderProgram *prog, const char *targetName)
    {
        if (prog && targetName)
        {
            target = prog->uniformLocation(targetName);
        }

        if (target < 0)
        {
            qDebug() << "Unable to set uniform location" << (targetName ? targetName : "UNKNOWN")
                     << ", value is " << target << "!";
        }

        return target >= 0;
    }

}
