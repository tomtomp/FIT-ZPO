/**
 * @file SceneCamera.h
 * @author Tomas Polasek
 * @brief Scene camera class.
 */

#include "SceneCamera.h"

namespace Util
{
    void SceneCamera::setPos(const QVector3D &pos)
    {
        setPos(pos.x(), pos.y(), pos.z());
    }

    void SceneCamera::setPos(float x, float y, float z)
    {
        mTranslation.setToIdentity();
        mTranslation.translate(x, y, z);
    }

    void SceneCamera::setRot(const QQuaternion &rot)
    {
        mRotation.setToIdentity();
        mRotation.rotate(rot);
    }

    void SceneCamera::setPerspective(float fov, float aspect, float nearPlane, float farPlane)
    {
        mProjection.setToIdentity();
        mProjection.perspective(fov, aspect, nearPlane, farPlane);
    }

    void SceneCamera::setOrtho(float l, float r, float b, float t, float nearPlane, float farPlane)
    {
        mProjection.setToIdentity();
        mProjection.ortho(l, r, b, t, nearPlane, farPlane);
    }

    void SceneCamera::recalculate()
    {
        calculateView();
        calculateViewProjection();
    }
}
