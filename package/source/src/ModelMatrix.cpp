/**
 * @file ModelMatrix.h
 * @author Tomas Polasek
 * @brief Scene camera class.
 */

#include "ModelMatrix.h"

namespace Util
{
    void ModelMatrix::recalculate()
    {
        mModel.setToIdentity();
        mModel.translate(mPosition);
        mModel.rotate(mRotation);
        mModel.scale(mScale);
    }
}
