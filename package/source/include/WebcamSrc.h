/**
 * @file WebcamSrc.h
 * @author Tomas Polasek
 * @brief Class acting as interface between the application and Qt Camera.
 */

#ifndef WEBCAMSRC_H
#define WEBCAMSRC_H

#include "Types.h"
#include "RenderArea.h"

#include <QCamera>
#include <QCameraInfo>
#include <QCameraImageCapture>
#include <QAbstractVideoSurface>
#include <QVideoSurfaceFormat>
#include <QMediaRecorder>
#include <QMessageBox>

/// Namespace containing utility functions and classes.
namespace Util
{
    class WebcamSrc;

    /// Fake destination for web camera, which we use to pass presented frames.
    class FakeVideoSurface : public QAbstractVideoSurface
    {
        Q_OBJECT

    public:
        /// Initialize fake surface.
        FakeVideoSurface(WebcamSrc *parent);
        ~FakeVideoSurface();

        /**
         * Get list of supported pixel formats.
         * @param handleType Handle used for passing frames, we want GLTextureHandle .
         * @return Returns list of supported formats for given handle.
         */
        virtual QList<QVideoFrame::PixelFormat>
        supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const override;

        //virtual bool isFormatSupported(const QVideoSurfaceFormat &format) const;
        //virtual QVideoSurfaceFormat nearestFormat(const QVideoSurfaceFormat &format) const;

        /// Called when capture is started.
        virtual bool start(const QVideoSurfaceFormat &format) override;
        /// Called when capture is stopped.
        virtual void stop() override;

        /**
         * Called when a new frame is presented.
         * @param frame Frame being presented.
         * @return Returns true, if successful, else returns false.
         */
        virtual bool present(const QVideoFrame &frame) override;

        /// Set whether this surface should present incoming frames.
        void setPresent(bool present)
        { mPresent = present; }
    private:
        /// Destination for passing presented frames.
        WebcamSrc *mDest;
        /// Flag specifying whether this surface should present incoming frames.
        bool mPresent;
    protected:
    }; // class FakeVideoSurface

    /// Qt Camera interface for use with LenseEngine. Inspired by Qt Camera example.
    class WebcamSrc : public QObject
    {
        Q_OBJECT

    public:
        /**
         * Initialize default camera.
         * @param renderArea Render area which will display the images.
         * @param parent Parent in the Qt object hierarchy.
         */
        explicit WebcamSrc(RenderArea *renderArea, QObject *parent = nullptr);
        virtual ~WebcamSrc();

        /// Get list of available cameras.
        const QList<QCameraInfo> availableCameras() const;

        /**
         * Set current camera to the one specified.
         * @param info Chosen camera.
         */
        void setCamera(const QCameraInfo &info);

        /// Set current camera to the default one.
        void setDefaultCamera();

        /// Start chosen camera.
        void startCamera();

        /// Stop chosen camera.
        void stopCamera();

        /// Start capturing video frames.
        void startCapture();

        /// Stop capturing video frames.
        void stopCapture();

        /// Capture a single image from camera.
        void captureImage();

        /// Present given image.
        void presentImage(QImage &&image);

        /// Present given video frame.
        void presentVideo(QImage &&frame);

        /// Does current camera support video capture?
        bool supportsVideoCapture() const;

        /// Does current camera support taking single images?
        bool supportsImageCapture() const;
    signals:
        /// Passthrough signal when camera changes state.
        void cameraStateChanged(QCamera::State);
        /// Passthrough signal when readiness for capturing images changes.
        void readyForCaptureChanged(bool);
    private:
        /**
         * Use given camera as current.
         * @param info Information about chosen camera.
         */
        void useCamera(const QCameraInfo &info);

        /// Callback when camera error occurs.
        void displayCameraError() const;

        /// Callback when image recorder error occurs.
        void displayImageError(int id, const QCameraImageCapture::Error error, const QString &errorString) const;

        /// Callback when camera captures requested image.
        void imageCaptured(int reqId, const QImage &image);

        void imageAvailable(int reqId, const QVideoFrame &frame);

        /// Renderint area used as destination for the images.
        RenderArea *mRenderArea;
        /// Fake video surface used for passing frames from camera to this class.
        FakeVideoSurface *mFakeSurface;
        /// Flaag specifying whether format of mFakeSurface changed.
        bool mVideoFormatChanged;
        /// Camera instance used for capturing images.
        QScopedPointer<QCamera> mCamera;
        /// Used for capturing single images.
        QScopedPointer<QCameraImageCapture> mImageCapture;
    protected:
    };// class WebcamSrc
} // namespace Util

#endif // WEBCAMSRC_H
