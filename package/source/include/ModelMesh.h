/**
 * @file ModelMesh.h
 * @author Tomas Polasek
 * @brief Model mesh loadable from a file.
 */

#ifndef MODELMESH_H
#define MODELMESH_H

#include "Types.h"
#include "Renderable.h"

#include <QFile>
#include <cstring>

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Base class for all lense objects in the scene.
    class ModelMesh : public Renderable
    {
    public:
        /**
         * Load model from given .ply file.
         * @param input Input file.
         */
        ModelMesh(QFile &input);

        /**
         * Load model from given .ply file.
         * @param input Input file.
         * @warning The mesh needs to be destroyed and re-created.
         */
        void loadModel(QFile &input);

        /**
         * Create all necessary buffers and fill them.
         * The target context has to be already current.
         */
        virtual void create() override;

        /**
         * Render this object in current context.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void render() override;

        /**
         * Destroy OpenGL objects, they can be
         * re-created with create function.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void destroy() override;
    private:
        /// Helper structure used for access to vertex components.
        struct alignas(GLfloat) VertexView
        {
            /// Position: x, y, z
            GLfloat pos[3];
            /// Normal: nx, ny, nz
            GLfloat normal[3];
            /// UV: u, v
            GLfloat uv[2];
        }; // struct VertexView

        /**
         * Components per one vertex:
         *   Position: 3 floats
         *   Normal:   3 floats
         *   UV coords:2 floats
         */
        static constexpr std::size_t COMP_PER_VERTEX{3 + 3 + 2};
        static_assert(sizeof(VertexView) == COMP_PER_VERTEX * sizeof(GLfloat), "Should be equal");

        /// Parse line and add vertex data.
        VertexView parseLine(const QByteArray &line);
        /// Parse line and add faces.
        void addFaces(const QByteArray &line);
        /// Does given string start with searched string?
        bool strStartsWith(const std::string &str, const std::string &search);

        /// The vertex array ID.
        GLuint mVa;
        /// The vertex buffer ID.
        GLuint mVb;
        /// The index buffer ID.
        GLuint mEb;

        /// Buffer containing the vertex data.
        std::vector<VertexView> mVertexData;
        /// Buffer containing the indices used for indexing vertex data.
        std::vector<GLuint> mIndexData;
    protected:
    }; // class SphericalLense
} // namespace Util

#endif // MODELMESH_H
