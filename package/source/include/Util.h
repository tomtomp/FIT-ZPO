/**
 * @file Util.h
 * @author Tomas Polasek
 * @brief Utility function and classes.
 */

#ifndef UTIL_H
#define UTIL_H

#include "Types.h"

#include <cmath>

/// Namespace containing utility functions and classes.
namespace Util
{
    /**
     * Convert message type to string representation.
     * @param msgType Message type from debug callback.
     * @return Returns string representation of the type.
     */
    const char *glMessageTypeToStr(GLenum msgType);

    /**
     * Convert message severity to string representation.
     * @param msgSeverity Message severity from debug callback.
     * @return Returns tring representation of the severity.
     */
    const char *glMessageSeverityToStr(GLenum msgSeverity);

    /// Calculate distance between 2 points.
    double distance(double x1, double y1, double x2, double y2);
} // namespace Util

#endif // UTIL_H
