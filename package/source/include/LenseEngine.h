/**
 * @file LenseEngine.h
 * @author Tomas Polasek
 * @brief Engine containing the scene and lense settings.
 */

#ifndef LENSEENGINE_H
#define LENSEENGINE_H

#include "Types.h"
#include "Plane.h"
#include "ImageSrc.h"
#include "DummyLense.h"
#include "SphericalLense.h"
#include "ModelLense.h"
#include "Material.h"
#include "SceneCamera.h"
#include "Keyboard.h"
#include "Mouse.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLFramebufferObject>

/// Namespace containing classes and functions used in the Lense engine.
namespace Engine
{
    class LenseEngine : public QObject
    {
        Q_OBJECT

    public:
        /// Uniform location used for source image sampler.
        static constexpr GLuint UNIFORM_LOC_SRC_SAMPLER{0};
        /// Uniform location used for mvp matrix.
        static constexpr GLuint UNIFORM_LOC_MVP{1};
        /// Uniform location used for inverse model matrix.
        static constexpr GLuint UNIFORM_LOC_INVMT{2};

        /**
         * Create structures and initialize.
         * @param width Starting viewport width.
         * @param height Starting viewport height.
         * @param parent Parent in the Qt object hierarchy.
         */
        explicit LenseEngine(int width, int height, QObject *parent = nullptr);
        /// Free used resources.
        virtual ~LenseEngine();

        /**
         * Update the scene how it should look in deltaT milliseconds.
         * @param deltaT Change time in milliseconds.
         */
        void update(float deltaT);

        /**
         * Set size of the viewport in pixels.
         * @param width Width of the viewport.
         * @param height Height of the viewport.
         */
        void setViewSize(int width, int height);

        /**
         * Render the current state of the scene.
         */
        void render();

        /// Set source image to the default testing pattern.
        void setDefaultSourceImage();

        /**
         * Set new source image.
         * @param image Image to use.
         * @param destroy Should the texture be destroyed and re-created?
         */
        void setSourceImage(QImage &&image, bool destroy);

        void setImageSize(float size)
        { mSrcImage->setScale(size); }
        void setImageRotation(float rotX, float rotY, float rotZ)
        { mSrcImage->setEulerRotation({rotX, rotY, rotZ}); }
        void setImagePosition(float posX, float posY, float posZ)
        { mSrcImage->setPosition({posX, posY, posZ}); }

        void setCameraPosition(float posX, float posY, float posZ)
        { mCamera.setPos(posX, posY, posZ); }
        void setCameraProjection(float fov);
        void setCameraOrthographic();

        void setLenseSize(float size)
        { mLense->setScale(size); }
        void setLenseRotation(float rotX, float rotY, float rotZ)
        { mLense->setEulerRotation(rotX, rotY, rotZ); }
        void setLensePosition(float posX, float posY, float posZ)
        { mLense->setPosition({posX, posY, posZ}); }
        void setLenseRefrIndex(float refrIndex)
        { mLenseMaterial.setBaseRefractiveIndex(refrIndex); }
        void setLenseChromaticAberration(bool enabled, float abbe);
        void setLenseTypeCube();
        void setLenseTypeSphere(float foc1, float radius1, float foc2, float radius2, float thickness, std::size_t rings, std::size_t sectors);
        void setLenseModel(QFile &input);

        void setEnvRefrIndex(float refrIndex)
        { mEnvMaterial.setBaseRefractiveIndex(refrIndex); }

        // Dispatch functions for user input.
        void dispatchKeyboard(Qt::Key key, Qt::KeyboardModifiers mods, Util::Keyboard::Action action)
        { mKeyboard.dispatch(key, mods, action); }
        void dispatchMouse(Qt::MouseButton key, Qt::KeyboardModifiers mods, Util::Mouse::Action action, double x, double y)
        { mMouse.dispatchMouse(key, mods, action, x, y); }
        void dispatchScroll(Qt::KeyboardModifiers mods, double xOffset, double yOffset, double x, double y)
        { mMouse.dispatchScroll(mods, xOffset, yOffset, x, y); }
        void dispatchPosition(double x, double y)
        { mMouse.dispatchPosition(x, y); }

    signals:
        /// Emitted when rotation of the lense changes.
        void rotationChanged(const QVector3D &newEulerAngles);
        /// Emitted when position of the lense changes.
        void positionChanged(const QVector3D &newPosition);
    private:
        /// Uniform locations of the shaders.
        struct UniformLocations
        {
            /**
             * Check location and set it to given target.
             * @param target Target to set to given location.
             * @param prog Shader program to get the location from.
             * @param targetName Name of the target in the shader program.
             * @return Returns true, if the name has been found.
             */
            static bool checkSet(GLint &target, QOpenGLShaderProgram *prog, const char *targetName);

            GLint imageSrcSampler;
            GLint imageMvp;

            GLint lenseMode;
            GLint lenseMap;
            GLint lenseBackground;
            GLint lenseMv;
            GLint lenseMvp;
            GLint lenseInvmt;
            GLint lenseP;
            GLint lenseEnvRI;
            GLint lenseRI;
            GLint lenseBgMvInv;

            GLint lenseCMode;
            GLint lenseCMap;
            GLint lenseCBackground;
            GLint lenseCMv;
            GLint lenseCMvp;
            GLint lenseCInvmt;
            GLint lenseCP;
            GLint lenseCEnvRI;
            GLint lenseCRI;
            GLint lenseCBgMvInv;

            GLint depthNormMv;
            GLint depthNormMvp;
            GLint depthNormInvmt;
        }; // struct UniformLocations

        /// Variable sandbox, used for testing stuff.
        struct Sandbox
        {
        };

        /// Number of display modes.
        static constexpr int NUM_MODES{8};

        /// Initialize OpenGL objects.
        void initGl();

        /// Initialize user input bindings.
        void initBindings();

        /// Initialize camera position and size.
        void initCamera();

        /// Recalculate camera projection matrix using members values.
        void refreshCamera();

        /// Flag specifying whether to use perspective camera projection.
        bool mProjectionPerspective;
        /// Width of the viewport.
        int mViewportWidth;
        /// Height of the viewport.
        int mViewportHeight;
        /// Field of view used for perspective camera.
        float mProjectionFOV;
        /// Shader program used for rendering the image.
        QOpenGLShaderProgram *mImageProg;
        /// Shader program used for rendering the lense.
        QOpenGLShaderProgram *mLenseProg;
        /// Shader program used for rendering the lense with chromatic aberration.
        QOpenGLShaderProgram *mLenseChromaProg;
        /// Shader program used for getting the depth and normal of back side of the lense.
        QOpenGLShaderProgram *mDepthNormProg;
        /// Uniform locations of the shader uniforms.
        UniformLocations mUl;
        /// Framebuffer used for offscreen rendering.
        QScopedPointer<QOpenGLFramebufferObject> mFb;
        /// Framebuffer used for offscreen rendering of the scene.
        QScopedPointer<QOpenGLFramebufferObject> mSceneFb;

        /// Camera matrix handler.
        Util::SceneCamera mCamera;
        /// Source image being displayed.
        QScopedPointer<Util::ImageSrc> mSrcImage;
        /// Lense in the scene.
        QScopedPointer<Util::Lense> mLense;
        /// Material definition of the lense.
        Util::Material mLenseMaterial;
        /// Material of the environment.
        Util::Material mEnvMaterial;

        /// Base rotation used for calculating the mouse rotation.
        QQuaternion mBaseRotation;
        /// Vector containing positional delta of cursor when rotating lense.
        QVector2D mRequestRotation;
        /// Flag specifying whether the rotaion of the lense is being changed.
        bool mChangingRotation;
        /// Position change from user scrolling.
        float mPosChange;
        /// Modes used for displaying different information.
        int mMode;
        /// Toggle for wireframe display of the lense.
        bool mWireFrame;

        /// Keyboard handler.
        Util::Keyboard mKeyboard;
        /// Mouse handler.
        Util::Mouse mMouse;

        /// Sandbox used for testing purposes.
        Sandbox mSandbox;
    protected:
    }; // class LenseEngine
} // namespace Engine

#endif // LENSEENGINE_H
