/**
 * @file DummyLense.h
 * @author Tomas Polasek
 * @brief This lense is just a dummy used for debugging purposes.
 */

#ifndef DUMMYLENSE_H
#define DUMMYLENSE_H

#include "Types.h"
#include "Lense.h"
#include "Cube.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Base class for all lense objects in the scene.
    class DummyLense : public Lense
    {
    public:
        DummyLense() = default;
        DummyLense(const SceneObject &other) :
            Lense(other)
        { }
        virtual ~DummyLense() = default;

        /**
         * Create the OpenGL resources used in rendering of this image.
         */
        virtual void create() override;

        /**
         * Render the source image.
         * @warning The shader program should already be bound, when called!
         */
        virtual void render() override;

        /**
         * Destroy OpenGL objects, they can be
         * re-created with create function.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void destroy() override;
    private:
        /// Cube representing the lense.
        Util::Cube mCube;
    protected:
    }; // class DummyLense
} // namespace Util

#endif // DUMMYLENSE_H
