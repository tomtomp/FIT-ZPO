/**
 * @file ModelLense.h
 * @author Tomas Polasek
 * @brief Lense using model from a file.
 */

#ifndef MODELLENSE_H
#define MODELLENSE_H

#include "Types.h"
#include "Lense.h"
#include "ModelMesh.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Base class for all lense objects in the scene.
    class ModelLense : public Lense
    {
    public:
        ModelLense(QFile &input);
        ModelLense(const SceneObject &other, QFile &input);

        virtual ~ModelLense() = default;

        /**
         * Create the OpenGL resources used in rendering of this image.
         */
        virtual void create() override;

        /**
         * Render the source image.
         * @warning The shader program should already be bound, when called!
         */
        virtual void render() override;

        /**
         * Destroy OpenGL objects, they can be
         * re-created with create function.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void destroy() override;
    private:
        /// Lense model.
        Util::ModelMesh mModel;
    protected:
    }; // class SphericalLense
} // namespace Util

#endif // MODELLENSE_H
