/**
 * @file Plane.h
 * @author Tomas Polasek
 * @brief Simple plane renderable in OpenGL.
 */

#ifndef PLANE_H
#define PLANE_H

#include "Types.h"
#include "Renderable.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Renderable plane.
    class Plane : public Renderable
    {
    public:
        /**
         * Set the object to default state.
         * @warning Does NOT create the buffers!
         */
        Plane();
        /// Destroy the buffers.
        virtual ~Plane();

        /**
         * Create all necessary buffers and fill them.
         * The target context has to be already current.
         */
        virtual void create() override;

        /**
         * Render this object in current context.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void render() override;

        /**
         * Destroy OpenGL objects, they can be
         * re-created with create function.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void destroy() override;
    private:
        /// 2 triangles for a quad.
        static constexpr unsigned int NUM_TRIANGLES{2};
        /// 3 vertices per triangle.
        static constexpr unsigned int NUM_VERTICES{NUM_TRIANGLES * 3};
        /// Position, Normal and UV.
        static constexpr unsigned int VALS_PER_VERTEX{3 + 3 + 2};

        /// Vertex data for the plane.
        static const GLfloat VERTEX_BUFFER_DATA[NUM_VERTICES * VALS_PER_VERTEX];

        /// Vertex array used for storing plane vertex information.
        GLuint mVa;
        /// Vertex buffer used as storage for vertex data.
        GLuint mVb;
    protected:
    }; // class Plane
} // namespace Util

#endif //PLANE_H
