/**
 * @file Mouse.h
 * @author Tomas Polasek
 * @brief Mouse handling class.
 */

#ifndef MOUSE_H
#define MOUSE_H

#include "Types.h"
#include "Util.h"

#include <QKeySequence>

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Mouse handling class.
    class Mouse
    {
    public:
        /// Actions possible with mouse buttons.
        enum Action
        {
            ACTION_PRESS = 0,
            ACTION_RELEASE,
            // Special action used for dragging.
            ACTION_DRAG,
        };

        /// Type of the action function, called when corresponding key is pressed, parameters - x and y.
        using ActionFun = std::function<void(double, double)>;
        /// Drag start function, parameters x and y;
        using DragStartFun = ActionFun;
        /// Drag ongoing function, parameters startX, startY, currentX and currentY.
        using DragFun = std::function<void(double, double, double, double)>;
        /// Drag end function, parameters startX, startY, currentX and currentY.
        using DragEndFun = DragFun;
        /// Type of the default action, parameters - key, mods, action, x and y.
        using DefaultActionFun = std::function<void(Qt::MouseButton, Qt::KeyboardModifiers, Action, double, double)>;
        /// Action taken, when scroll occurs, parameters - mods, xOffset, yOffset, x and y.
        using ScrollActionFun = std::function<void(Qt::KeyboardModifiers, double, double, double, double)>;
        /// Action taken, when mouse position is taken, parameters - x and y.
        using MousePosActionFun = std::function<void(double, double)>;

        Mouse() = default;
        ~Mouse() = default;

        /**
         * Set action for given key combination.
         * @param key Key - e.g. Qt::LeftButton.
         * @param mods Modifiers - e.g. Qt::ShiftModifier.
         * @param action Action taken with the key - e.g. Mouse::ACTION_PRESS.
         * @param fun Function to call, function will receive x and y mouse positions.
         */
        void setAction(Qt::MouseButton key, Qt::KeyboardModifiers mods, Action action, ActionFun fun)
        { mMapping[{key, mods, action}] = fun; }

        /**
         * Reset action for given key combination.
         * @param key Key - e.g. Qt::LeftButton.
         * @param mods Modifiers - e.g. Qt::ShiftModifier.
         * @param action Action taken with the key - e.g. Mouse::ACTION_PRESS.
         */
        void setAction(Qt::MouseButton key, Qt::KeyboardModifiers mods, Action action)
        { mMapping.erase({key, mods, action}); }

        /**
         * Set the default action, called when no other mapping is found.
         * Function will be passed following arguments :
         *  int key - Key pressed, e.g. Qt::LeftButton.
         *  int mods - Mods - e.g. Qt::ShiftModifier.
         *  int action - Action of the key - e.g. Mouse::ACTION_PRESS.
         * @param fun Function to call.
         */
        void setDefaultAction(DefaultActionFun fun)
        { mDefaultAction = fun; }

        /**
         * Set action for given dragging combination.
         * @param key Key - e.g. Qt::LeftButton.
         * @param mods Modifiers - e.g. Qt::ShiftModifier.
         * @param deadZone How far has the cursor move before starting the drag with original start.
         * @param start Function called when drag starts, out of deadZone. Parameters x and y.
         * @param drag Function for continuous drag, parameters startX, startY, currentX and currentY.
         * @param end Function for ending the drag, parameters startX, startY, currentX and currentY.
         * @warning Any of the functions are triggered only if the mouse is further than deadZone!
         */
        void setDragAction(Qt::MouseButton key, Qt::KeyboardModifiers mods, double deadZone,
                           DragStartFun start, DragFun drag, DragEndFun end)
        { mDragMapping[{key, mods, ACTION_DRAG}] = {start, drag, end, deadZone}; }

        /**
         * Reset action for given dragging combination.
         * @param key Key - e.g. Qt::LeftButton.
         * @param mods Modifiers - e.g. Qt::ShiftModifier.
         */
        void setDragAction(Qt::MouseButton key, Qt::KeyboardModifiers mods);

        /**
         * Reset the default action.
         */
        void resetDefaultAction()
        { mDefaultAction = nullptr; }

        /**
         * Set function called, when scrolling occurs
         * @param fun Function to call, function will
         * receive modifiers, x and y scroll offset
         * and x and y mouse position.
         */
        void setScrollAction(ScrollActionFun fun)
        { mScrollFun = fun; }

        /**
         * Reset the scroll action.
         */
        void resetScrollAction()
        { mScrollFun = nullptr; }

        /**
         * Set function called, when mouse position is received.
         * @param fun Function to call, x and y coordinates are passed as double values.
         */
        void setMousePosAction(MousePosActionFun fun)
        { mMousePosFun = fun; }

        /**
         * Reset the mouse position callback.
         */
        void resetMousePosAction()
        { mMousePosFun = nullptr; }

        /**
         * Dispatch mouse action to correct callback.
         * @param key Key - e.g. Qt::LeftButton.
         * @param mods Modifiers - e.g. Qt::ShiftModifier.
         * @param action Action taken with the key - e.g. Mouse::ACTION_PRESS.
         * @param fun Function to call, function will receive x and y mouse positions.
         */
        void dispatchMouse(Qt::MouseButton key, Qt::KeyboardModifiers mods, Action action, double x, double y);

        /**
         * Dispatch mouse wheel action to correct callback.
         * @param mods Modifiers - e.g. Qt::ShiftModifier.
         * @param xOffset X offset of the mouse wheel.
         * @param yOffset Y offset of the mouse wheel.
         * @param x X position of the mouse.
         * @param y Y position of the mouse.
         */
        void dispatchScroll(Qt::KeyboardModifiers mods, double xOffset, double yOffset, double x, double y);

        /**
         * Dispatch mouse position to correct callback.
         * @param x X position of the mouse.
         * @param y Y position of the mouse.
         */
        void dispatchPosition(double x, double y);

        /// Print debug information about given button press.
        static void debugPrintButtonAction(Qt::MouseButton button, Qt::KeyboardModifiers mods, Action action, double x, double y);

        /**
         * Convert action to string representation.
         * @param action Action to convert to string.
         * @return Returns string representation of provided action.
         */
        static const char *actionToStr(Action action);
    private:
        /// Helper structure for searching in map.
        struct KeyCombination
        {
            KeyCombination() = default;

            KeyCombination(Qt::MouseButton keyV, Qt::KeyboardModifiers modsV, Action actionV = ACTION_DRAG) :
                key{keyV}, mods{modsV}, action{actionV} { }

            /// Keycode.
            Qt::MouseButton key;
            /// Modifiers.
            Qt::KeyboardModifiers mods;
            /// Action - e.g. Mouse::ACTION_PRESS.
            Action action;

            /// Comparison operator.
            bool operator<(const KeyCombination &rhs) const
            { return (key < rhs.key) || ((key == rhs.key) && (mods < rhs.mods)) || ((key == rhs.key) && (mods == rhs.mods) && (action < rhs.action)); }
            /// Comparison equal operator.
            bool operator==(const KeyCombination &rhs) const
            { return (key == rhs.key) && (mods == rhs.mods) && (action == rhs.action); }
        }; // struct KeyCombination

        /// Helper structure combining dragging functions and deadzone.
        struct DragFunctions
        {
            DragFunctions() = default;

            DragFunctions(DragStartFun startF, DragFun dragF, DragEndFun endF, double deadZoneV):
                start{startF}, drag{dragF}, end{endF}, deadZone{deadZoneV}
            { }

            /// Function called when drag starts.
            DragStartFun start;
            /// Function called when dragging.
            DragFun drag;
            /// Function called when drag ends.
            DragEndFun end;
            /// How long does the drag have to be to start.
            double deadZone;
        }; // struct DragFun

        /// Container for information about currently ongoing drag.
        struct CurrentDrag
        {
            DragFunctions dragFun;
            bool dragging{false};
            KeyCombination keyCombination;

            /// Starting x position of the drag.
            double startX;
            /// Starting y position of the drag.
            double startY;

            bool started{false};
        }; // struct CurrentDrag

        /// Process possible drag event.
        void processDragButton(Qt::MouseButton button, Qt::KeyboardModifiers mods, Action action, double x, double y);

        /// Process new cursor position for drag events.
        void processDragPosition(double x, double y);

        /// Mapping from keys to actions.
        std::map<KeyCombination, ActionFun> mMapping;
        /// Mapping for drag functions.
        std::map<KeyCombination, DragFunctions> mDragMapping;
        /// Currently started drag.
        CurrentDrag mCurrentDrag;
        /// Function called, when scrolling occurs.
        ScrollActionFun mScrollFun;
        /// Function called, when mouse position is received.
        MousePosActionFun mMousePosFun;
        /// Default action, called when no mapping is found.
        DefaultActionFun mDefaultAction;
    protected:
    }; // class Mouse
} // namespace Util

#endif //MOUSE_H
