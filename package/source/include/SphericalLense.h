/**
 * @file SphericalLense.h
 * @author Tomas Polasek
 * @brief Parametrized spherical lense.
 */

#ifndef SPHERICALLENSE_H
#define SPHERICALLENSE_H

#include "Types.h"
#include "Lense.h"
#include "ParamSphere.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Base class for all lense objects in the scene.
    class SphericalLense : public Lense
    {
    public:
        SphericalLense(float foc1, float radius1,
                       float foc2, float radius2,
                       float thickness,
                       std::size_t rings, std::size_t sectors);
        SphericalLense(const SceneObject &other,
                       float foc1, float radius1,
                       float foc2, float radius2,
                       float thickness,
                       std::size_t rings, std::size_t sectors);
        virtual ~SphericalLense() = default;

        /**
         * Create the OpenGL resources used in rendering of this image.
         */
        virtual void create() override;

        /**
         * Render the source image.
         * @warning The shader program should already be bound, when called!
         */
        virtual void render() override;

        /**
         * Destroy OpenGL objects, they can be
         * re-created with create function.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void destroy() override;
    private:
        /// Parametrized sphere model.
        Util::ParamSphere mModel;
    protected:
    }; // class SphericalLense
} // namespace Util

#endif // SPHERICALLENSE_H
