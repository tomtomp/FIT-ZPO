/**
 * @file LenseSimulatorApp.cpp
 * @author Tomas Polasek
 * @brief Main application class.
 */

#include "Types.h"
#include "MainWindow.h"

#include <QApplication>

#ifndef LENSESIMULATORAPP_H
#define LENSESIMULATORAPP_H

class LenseSimulatorApp : public QApplication
{
    Q_OBJECT

public:
    LenseSimulatorApp(int &argc, char **argv);
    ~LenseSimulatorApp();

    /// The main application loop.
    int start();
private:
    /// The main application window.
    MainWindow mWindow;
protected:
}; // class LenseSimulatorApp

#endif // LENSESIMULATORAPP_H
