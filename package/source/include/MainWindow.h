/**
 * @file MainWindow.h
 * @author Tomas Polasek
 * @brief Main window of the application.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "Types.h"
#include "WebcamSrc.h"

#include <QMainWindow>
#include <QFileDialog>
#include <QStandardPaths>
#include <QImageReader>
#include <QImageWriter>
#include <QMessageBox>

namespace Ui
{
    class MainWindow;
} // namespace Ui

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /// Connect menu item actions to functions.
    void setupMenuActions();

    /// Connect widgets to corresponding actions in the image tab.
    void setupImageTab();

    /// Connect widgets to corresponding actions in the camera tab.
    void setupCameraTab();

    /// Connect widgets to corresponding actions in the lense tab.
    void setupLenseTab();

    /// Connect widgets to corresponding actions in the environment tab.
    void setupEnvironmentTab();

    /// Connect buttons to corresponding actions in the webcam tab.
    void setupWebcamTab();

    /// Connect engine signals to the UI.
    void setupEngineConnection();

    /**
     * Ask the user to select image file/s.
     * @param acceptMode Accept mode - open or save.
     * @param fileMode File mode - single file, multiple files, existing etc.
     * @return Returns list of selected file names, or empty if user didn't choose any.
     */
    QStringList chooseImage(QFileDialog::AcceptMode acceptMode, QFileDialog::FileMode fileMode);

    /**
     * Ask the user to select model file.
     * @return Returns list of selected file names, or empty if user didn't choose any.
     */
    QStringList chooseModel();

    // Actions for individual menu items.
    void loadImage();
    void saveImage();
    void resetImage();
    void quit();

    // Actions for image tab.
    void setImageSize(double size);
    void setImageRotation(int);
    void setImagePosition(double);

    // Actions for camera tab.
    void setCameraPosition(double);
    void setCameraMode(bool checked);
    void setCameraFOV(double fov);

    // Actions for lense tab.
    void setLenseSize(double size);
    void setLensePosition(double);
    void setLenseRotation(double);
    void setLenseRefrIndex(double newIndex);
    void setLenseCA(bool checked);
    void setLenseCAAbbe(double abbe);
    void setLenseShape(bool checked);
    void setSphLenseParamsD(double);
    void setSphLenseParamsI(int);
    void setSphLenseParamVals();
    void loadModel();

    // Actions for Environment tab.
    void setEnvRefrIndex(double newIndex);

    // Actions for webcam tab.
    void startCamera();
    void stopCamera();
    void startCapture();
    void takeSnapshot();

    // Actions for engine signals.
    void engineRotationChanged(const QVector3D &newEulerAngles);
    void enginePositionChanged(const QVector3D &newPosition);

    /// Called when engine has been initialized.
    void engineInitialized();

    /// Callback when camera changes state.
    void cameraStateChanged(QCamera::State state);
    /// Callback when camera is ready to capture image.
    void readyForCaptureChanged(bool ready);
private:
    /// Main ui pointer.
    Ui::MainWindow *mUi;
    /// Interface between camera and application.
    Util::WebcamSrc *mWebcamSrc;
}; // class MainWindow

#endif // MAINWINDOW_H
