/**
 * @file Keyboard.cpp
 * @author Tomas Polasek
 * @brief Keyboard handling class.
 */

#include "Keyboard.h"

namespace Util
{
    void Keyboard::dispatch(Qt::Key key, Qt::KeyboardModifiers mods, Util::Keyboard::Action action)
    {
        decltype(mMapping.begin()) search{mMapping.find({key, mods, action})};
        if (search != mMapping.end())
        { // We found an action!
            search->second();
        }
        else if (mDefaultAction)
        {
            mDefaultAction(key, mods, action);
        }
    }

    void Keyboard::debugPrintKeyAction(Qt::Key key, Qt::KeyboardModifiers mods, Util::Keyboard::Action action)
    {
        qDebug() << "Key: " << QKeySequence(key).toString()
                 << "; Mods: " << QKeySequence(mods).toString()
                 << "; Action: " << actionToStr(action);
    }

    const char *Keyboard::actionToStr(Keyboard::Action action)
    {
        switch (action)
        {
            case ACTION_PRESS:
            {
                return "Press";
            }
            case ACTION_RELEASE:
            {
                return "Release";
            }
            case ACTION_REPEAT:
            {
                return "Repeat";
            }
            default:
            {
                break;
            }
        }
        return "Unknown";
    }
}
