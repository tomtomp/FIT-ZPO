/**
 * @file ParamSphere.cpp
 * @author Tomas Polasek
 * @brief Parametrized sphere model.
 */

#include "ParamSphere.h"

#define _USE_MATH_DEFINES
#include "math.h"

namespace Util
{
    ParamSphere::ParamSphere(float focus1, float radius1, float focus2, float radius2, float thickness, std::size_t rings, std::size_t sectors) :
        mVa{0u}, mVb{0u}, mEb{0u}
    {
        setParameters(focus1, radius1, focus2, radius2, thickness, rings, sectors);
    }

    ParamSphere::~ParamSphere()
    { destroy(); }

    void ParamSphere::setParameters(float focus1, float radius1, float focus2, float radius2, float thickness, std::size_t rings, std::size_t sectors)
    {
        mFocus1 = focus1;
        mRad1 = radius1;
        mFocus2 = focus2;
        mRad2 = radius2;
        mThickness = thickness;
        mRings = rings;
        mSectors = sectors;
    }

    void ParamSphere::create()
    {
        // Generate vertex and index data.
        generateSphere();

        // Create vertex array.
        gl.glGenVertexArrays(1, &mVa);
        if (!mVa)
        {
            throw std::runtime_error("Unable to glGenVertexArrays!");
        }
        gl.glBindVertexArray(mVa);

        // Create vertex buffer object.
        gl.glGenBuffers(1, &mVb);
        if (!mVb)
        {
            destroy();
            throw std::runtime_error("Unable to glGenBuffers!");
        }
        gl.glBindBuffer(GL_ARRAY_BUFFER, mVb);

        // Set the data.
        gl.glBufferData(GL_ARRAY_BUFFER, mVertexData.size() * sizeof(mVertexData[0]), mVertexData.data(), GL_STATIC_DRAW);

        // Create index buffer object.
        gl.glGenBuffers(1, &mEb);
        if (!mEb)
        {
            destroy();
            throw std::runtime_error("Unable to glGenBuffers!");
        }
        gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEb);

        // Set the data.
        gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndexData.size() * sizeof(mIndexData[0]), mIndexData.data(), GL_STATIC_DRAW);

        // Setup vertex puller.
        gl.glVertexAttribPointer(ATTRIBUTE_LOC_POS, 3, GL_FLOAT, GL_FALSE,
                                 sizeof(GLfloat) * COMP_PER_VERTEX,
                                 reinterpret_cast<void*>(sizeof(GLfloat) * 0));
        gl.glEnableVertexAttribArray(ATTRIBUTE_LOC_POS);
        gl.glVertexAttribPointer(ATTRIBUTE_LOC_NORM, 3, GL_FLOAT, GL_FALSE,
                                 sizeof(GLfloat) * COMP_PER_VERTEX,
                                 reinterpret_cast<void*>(sizeof(GLfloat) * 3));
        gl.glEnableVertexAttribArray(ATTRIBUTE_LOC_NORM);
        gl.glVertexAttribPointer(ATTRIBUTE_LOC_UV, 2, GL_FLOAT, GL_FALSE,
                                 sizeof(GLfloat) * COMP_PER_VERTEX,
                                 reinterpret_cast<void*>(sizeof(GLfloat) * 6));
        gl.glEnableVertexAttribArray(ATTRIBUTE_LOC_UV);

        // Unbind the buffers and array.
        gl.glBindBuffer(GL_ARRAY_BUFFER, 0);
        gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        gl.glBindVertexArray(0);

        // We can delete the buffers since VAO is holding a reference to it.
        //gl.glDeleteBuffers(1, &mEb);
        //mEb = 0u;
        gl.glDeleteBuffers(1, &mVb);
        mVb = 0u;

        mCreated = true;
    }

    void ParamSphere::render()
    {
        // Bind the vertex array.
        gl.glBindVertexArray(mVa);

        gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEb);

        // Draw the quads.
        gl.glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mIndexData.size()), GL_UNSIGNED_INT, nullptr);

        // Disable the vertex array.
        gl.glBindVertexArray(0);
    }

    void ParamSphere::destroy()
    {
        mCreated = false;

        if (mEb)
        {
            gl.glDeleteBuffers(1, &mEb);
            mEb = 0;
        }

        if (mVa)
        {
            gl.glDeleteVertexArrays(1, &mVa);
            mVa = 0;
        }

        if (mVb)
        {
            gl.glDeleteBuffers(1, &mVb);
            mVb = 0;
        }
    }

    void ParamSphere::generateSphere()
    {
        /*
         * Implementation inspired by user dantewofl on stackoverflow:
         *   https://stackoverflow.com/questions/5988686/creating-a-3d-sphere-in-opengl-using-visual-c
         */

        // Change per ring.
        const GLfloat DR{1.0f / (mRings - 1u)};
        // Change per sector.
        const GLfloat DS{1.0f / (mSectors - 1u)};
        // Change per width ring.
        const GLfloat WDR{1.0f / (WIDTH_BAND_RINGS - 1u)};
        // Change per width sector.
        const GLfloat WDS{1.0f / (mSectors - 1u)};
        const GLfloat BAND_WIDTH_HALF{mThickness / 2.0f};
        /*
         * Number of sectors in the whole parametrized sphere.
         * 2 deformed half-spheres :            2 * mRings * mSectors each +
         * ring in the middle for thickness:    mSectors
         */
        const std::size_t TOTAL_SECTORS{2 * mRings * mSectors + WIDTH_BAND_RINGS * mSectors};

        // Allocate space for given vertex size.
        mVertexData.resize(TOTAL_SECTORS * COMP_PER_VERTEX);
        // Allocte space for required number of indices.
        mIndexData.resize(TOTAL_SECTORS * INDICES_PER_PRIM);

        // View iterator.
        VertexView *vit{reinterpret_cast<VertexView*>(mVertexData.data())};

        // Front arc:
        // Where the arc starts on the sphere.
        float fArcStartTheta = acosf(1.0f / mFocus1);
        // Angle move per one sector.
        float fArcThetaDelta = M_PI_2 - fArcStartTheta;
        // Distance from the base of the sphere.
        float fArcDist = mFocus1 * sinf(fArcStartTheta);
        for (std::size_t r = 0; r < mRings; ++r)
        {
            for (std::size_t s = 0; s < mSectors; ++s)
            {
                //const GLfloat theta = M_PI_2 + M_PI_2 * r * DR;
                const GLfloat theta = M_PI_2 + fArcStartTheta + fArcThetaDelta * r * DR;
                //const GLfloat thetaHalf = M_PI_2 * r * DR;
                const GLfloat thetaHalf = fArcStartTheta + fArcThetaDelta * r * DR;
                const GLfloat phi = 2.0f * M_PI * s * DS;
                const GLfloat x = cos(phi) * sin(theta) * mFocus1;
                const GLfloat y = sin(phi) * sin(theta) * mFocus1;
                const GLfloat z = sin(thetaHalf) * mFocus1 - fArcDist;

                vit->pos[0] = x;
                vit->pos[1] = y;
                vit->pos[2] = z * mRad1 + BAND_WIDTH_HALF;

                // Multiplication with inverse transpose:
                QVector3D v(x, y, z / mRad1);
                // Re-normalization
                v.normalize();

                if (mRad1 > 0)
                {
                    vit->normal[0] = v.x();
                    vit->normal[1] = v.y();
                    vit->normal[2] = v.z();
                }
                else
                {
                    vit->normal[0] = -v.x();
                    vit->normal[1] = -v.y();
                    vit->normal[2] = -v.z();
                }

                vit->uv[0] = s * DS;
                vit->uv[1] = r * DR;

                vit++;
            }
        }

        // Width band:
        for (std::size_t r = 0; r < WIDTH_BAND_RINGS; ++r)
        {
            for (std::size_t s = 0; s < mSectors; ++s)
            {
                const GLfloat phi = 2.0f * M_PI * s * DS;
                const GLfloat x = cos(phi);
                const GLfloat y = sin(phi);

                vit->pos[0] = x;
                vit->pos[1] = y;
                vit->pos[2] = -BAND_WIDTH_HALF + mThickness * r * WDR;

                vit->normal[0] = x;
                vit->normal[1] = y;
                vit->normal[2] = 0.0f;

                vit->uv[0] = s * WDS;
                vit->uv[1] = r * WDR;

                vit++;
            }
        }

        // Back arc:
        // Where the arc starts on the sphere.
        float bArcStartTheta = -acosf(1.0f / mFocus2);
        // Angle move per one sector.
        float bArcThetaDelta = M_PI_2 + bArcStartTheta;
        // Distance from the base of the sphere.
        float bArcDist = mFocus2 * sinf(bArcStartTheta);
        for (std::size_t r = 0; r < mRings; ++r)
        {
            for (std::size_t s = 0; s < mSectors; ++s)
            {
                //const GLfloat theta = M_PI_2 * r * DR;
                const GLfloat theta = bArcThetaDelta * r * DR;
                //const GLfloat thetaHalf = -M_PI_2 + M_PI_2 * r * DR;
                const GLfloat thetaHalf = -M_PI_2 + bArcThetaDelta * r * DR;
                const GLfloat phi = 2.0f * M_PI * s * DS;
                const GLfloat x = cos(phi) * sin(theta) * mFocus2;
                const GLfloat y = sin(phi) * sin(theta) * mFocus2;
                const GLfloat z = sin(thetaHalf) * mFocus2 - bArcDist;

                vit->pos[0] = x;
                vit->pos[1] = y;
                vit->pos[2] = z * mRad2 - BAND_WIDTH_HALF;

                // Multiplication with inverse transpose:
                QVector3D v(x, y, z / mRad2);
                // Re-normalization
                v.normalize();

                if (mRad2 > 0)
                {
                    vit->normal[0] = v.x();
                    vit->normal[1] = v.y();
                    vit->normal[2] = v.z();
                }
                else
                {
                    vit->normal[0] = -v.x();
                    vit->normal[1] = -v.y();
                    vit->normal[2] = -v.z();
                }

                vit->uv[0] = s * DS;
                vit->uv[1] = r * DR;

                vit++;
            }
        }

        // Index iterator
        auto iit{mIndexData.begin()};
        // How many elements we already processed.
        std::size_t delta{0u};

        // Front arc:
        for (std::size_t r = 0; r < mRings - 1u; ++r)
        {
            for (std::size_t s = 0; s < mSectors - 1u; ++s)
            {
                // First triangle of the sector quad.
                *iit++ = static_cast<GLuint>(delta + r * mSectors + s);
                *iit++ = static_cast<GLuint>(delta + r * mSectors + (s + 1));
                *iit++ = static_cast<GLuint>(delta + (r + 1) * mSectors + (s + 1));

                // Second triangle of the sector quad.
                *iit++ = static_cast<GLuint>(delta + (r + 1) * mSectors + (s + 1));
                *iit++ = static_cast<GLuint>(delta + (r + 1) * mSectors + s);
                *iit++ = static_cast<GLuint>(delta + r * mSectors + s);
            }
        }
        delta += mRings * mSectors;

        // Width band:
        for (std::size_t r = 0; r < WIDTH_BAND_RINGS - 1u; ++r)
        {
            for (std::size_t s = 0; s < mSectors - 1u; ++s)
            {
                // First triangle of the sector quad.
                *iit++ = static_cast<GLuint>(delta + r * mSectors + s);
                *iit++ = static_cast<GLuint>(delta + r * mSectors + (s + 1));
                *iit++ = static_cast<GLuint>(delta + (r + 1) * mSectors + (s + 1));

                // Second triangle of the sector quad.
                *iit++ = static_cast<GLuint>(delta + (r + 1) * mSectors + (s + 1));
                *iit++ = static_cast<GLuint>(delta + (r + 1) * mSectors + s);
                *iit++ = static_cast<GLuint>(delta + r * mSectors + s);
            }
        }
        delta += WIDTH_BAND_RINGS * mSectors;

        // Back arc:
        for (std::size_t r = 0; r < mRings - 1u; ++r)
        {
            for (std::size_t s = 0; s < mSectors - 1u; ++s)
            {
                // First triangle of the sector quad.
                *iit++ = static_cast<GLuint>(delta + r * mSectors + s);
                *iit++ = static_cast<GLuint>(delta + r * mSectors + (s + 1));
                *iit++ = static_cast<GLuint>(delta + (r + 1) * mSectors + (s + 1));

                // Second triangle of the sector quad.
                *iit++ = static_cast<GLuint>(delta + (r + 1) * mSectors + (s + 1));
                *iit++ = static_cast<GLuint>(delta + (r + 1) * mSectors + s);
                *iit++ = static_cast<GLuint>(delta + r * mSectors + s);
            }
        }
        delta += mRings * mSectors;
    }
}
